﻿/*---------------------------------------------------------------------------------
Project:            1084

Program:            ____

Function:           ____

Dependencies:		____

Dependents:			____

Author:             ____

Date Completed:     ____

SAS Version:        ____   

Platform:           ____

Macros Called:      ____ 

Macro Parameters:   ____

Comments:           ____

Example:			____

Revisions:
date	name	what
211017	SH		Initial Version
---------------------------------------------------------------------------------*/

*libname DEF "S:\PAS14\DEF";
*libname DEMO "S:\PAS14\DEMO"; 
*libname CPT "S:\PAS14\CPT"; 
*libname DIAG "S:\PAS14\DIAG"; 
*libname PROC "S:\PAS14\PROC"; 
*libname BILL "S:\PAS14\BILL"; 
*libname VITALS "S:\PAS14\VITALS"; 
*libname GENLAB "S:\PAS14\GENLAB"; 
*libname LAB_RES "S:\PAS14\LAB_RES"; 
*libname LAB_SENS "S:\PAS14\LAB_SENS"; 
*libname READMIT "S:\PAS14\READMIT"; 



* ==================================  Step 1. DEF  ================================== *;
/*
ADMSRC|phd_admsrc.txt.gz|24|2
ADMTYPE|phd_admtype.txt.gz|7|2
CHGMSTR|phd_chgmstr.txt.gz|66474|17
CPTCODE|phd_cptcode.txt.gz|23507|2
DE_ADMIN|phd_de_admin.txt.gz|1|3
DISSTAT|phd_disstat.txt.gz|44|2
HOSPCHG|phd_hospchg.txt.gz|42600023|2
ICDCODE|phd_icdcode.txt.gz|316832|9
ICDPOA|phd_icdpoa.txt.gz|8|3
MSDRG|phd_msdrg.txt.gz|801|2
MSDRGMDC|phd_msdrgmdc.txt.gz|31|2
PATTYPE|phd_pattype.txt.gz|17|2
PAYOR|phd_payor.txt.gz|16|2
PHYSPEC|phd_physpec.txt.gz|136|2
POORIGIN|phd_poorigin.txt.gz|19|2
PROVIDERS|phd_providers.txt.gz|1230|7
*/

proc import datafile="F:\pas14\def\phd_admsrc.txt"
	out=DEF.ADMSRC (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 24, C = 2 *;

proc import datafile="F:\pas14\def\phd_admtype.txt"
	out=DEF.ADMTYPE (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 7, C = 2 *;

proc import datafile="F:\PAS14\def\phd_chgmstr.txt"
	out=DEF.CHGMSTR (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 66474, C = 17 *;

proc import datafile="F:\pas14\def\phd_cptcode.txt"
	out=DEF.CPTCODE (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 23507, C = 2 *;

proc import datafile="F:\pas14\def\phd_de_admin.txt"
	out=DEF.DE_ADMIN (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 1, C = 3 *;

proc import datafile="F:\pas14\def\phd_disstat.txt"
	out=DEF.DISSTAT (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 44, C = 2 *;

proc import datafile="F:\pas14\def\phd_hospchg.txt"
	out=DEF.HOSPCHG (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 42600023, C = 2 *;

proc import datafile="F:\pas14\def\phd_icdcode.txt"
	out=DEF.ICDCODE (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 316832, C = 9 *;

proc import datafile="F:\pas14\def\phd_icdpoa.txt"
	out=DEF.ICDPOA (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 8, C = 3 *;

proc import datafile="F:\pas14\def\phd_msdrg.txt"
	out=DEF.MSDRG (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 801, C = 2 *;

proc import datafile="F:\pas14\def\phd_msdrgmdc.txt"
	out=DEF.MSDRGMDC (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 31, C = 2 *;

proc import datafile="F:\pas14\def\phd_pattype.txt"
	out=DEF.PATTYPE (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 17, C = 2 *;

proc import datafile="F:\pas14\def\phd_payor.txt"
	out=DEF.PAYOR (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 16, C = 2 *;

proc import datafile="F:\pas14\def\phd_physpec.txt"
	out=DEF.PHYSPEC (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 136, C = 2 *;

proc import datafile="F:\pas14\def\phd_poorigin.txt"
	out=DEF.POORIGIN (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 19, C = 2 *;

proc import datafile="F:\pas14\def\phd_providers.txt"
	out=DEF.PROVIDERS (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 1230, C = 7 *;



* ==================================  Step 2. PATDEMO  ================================== *;
/*
PATDEMO|phd_20181_patdemo.txt.gz|23434818|31
PATDEMO|phd_20182_patdemo.txt.gz|23176219|31
PATDEMO|phd_20183_patdemo.txt.gz|22720107|31
PATDEMO|phd_20184_patdemo.txt.gz|22775654|31
PATDEMO|phd_20191_patdemo.txt.gz|23382219|31
PATDEMO|phd_20192_patdemo.txt.gz|23777726|31
PATDEMO|phd_20193_patdemo.txt.gz|23716110|31
PATDEMO|phd_20194_patdemo.txt.gz|23150517|31
PATDEMO|phd_20201_patdemo.txt.gz|21109872|31
PATDEMO|phd_20202_patdemo.txt.gz|14868326|31
PATDEMO|phd_20203_patdemo.txt.gz|20313961|31
PATDEMO|phd_20204_patdemo.txt.gz|20332065|31
*/

proc import datafile="F:\pas14\patdemo\phd_20181_patdemo.txt"
	out=DEMO.demo_20181 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 23434818, C = 31 (<2 min) *;

proc import datafile="F:\pas14\patdemo\phd_20182_patdemo.txt"
	out=DEMO.demo_20182 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 23176219, C = 31 *;

proc import datafile="F:\pas14\patdemo\phd_20183_patdemo.txt"
	out=DEMO.demo_20183 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 22720107, C = 31 *;

proc import datafile="F:\pas14\patdemo\phd_20184_patdemo.txt"
	out=DEMO.demo_20184 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 22775654, C = 31 *;

proc import datafile="F:\pas14\patdemo\phd_20191_patdemo.txt"
	out=DEMO.demo_20191 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 23382219, C = 31 *;

proc import datafile="F:\pas14\patdemo\phd_20192_patdemo.txt"
	out=DEMO.demo_20192 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 23777726, C = 31 *;

proc import datafile="F:\pas14\patdemo\phd_20193_patdemo.txt"
	out=DEMO.demo_20193 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 23716110, C = 31 *;

proc import datafile="F:\pas14\patdemo\phd_20194_patdemo.txt"
	out=DEMO.demo_20194 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 23150517, C = 31 *;

proc import datafile="F:\pas14\patdemo\phd_20201_patdemo.txt"
	out=DEMO.demo_20201 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 21109872, C = 31 *;

proc import datafile="F:\pas14\patdemo\phd_20202_patdemo.txt"
	out=DEMO.demo_20202 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 14868326, C = 31 *;

proc import datafile="F:\pas14\patdemo\phd_20203_patdemo.txt"
	out=DEMO.demo_20203 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 20313961, C = 31 *;

proc import datafile="F:\pas14\patdemo\phd_20204_patdemo.txt"
	out=DEMO.demo_20204 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 20332065, C = 31 *;

proc contents data=DEMO.demo_20181 position;
run;



* ==================================  Step 3. CPT  ================================== *;
/*
PATCPT|phd_20181_patcpt.txt.gz|117839336|2
PATCPT|phd_20182_patcpt.txt.gz|113719402|2
PATCPT|phd_20183_patcpt.txt.gz|115429083|2
PATCPT|phd_20184_patcpt.txt.gz|119842905|2
PATCPT|phd_20191_patcpt.txt.gz|126983876|2
PATCPT|phd_20192_patcpt.txt.gz|128995630|2
PATCPT|phd_20193_patcpt.txt.gz|131675849|2
PATCPT|phd_20194_patcpt.txt.gz|143202638|2
PATCPT|phd_20201_patcpt.txt.gz|129653042|2
PATCPT|phd_20202_patcpt.txt.gz|96419352|2
PATCPT|phd_20203_patcpt.txt.gz|124860195|2
PATCPT|phd_20204_patcpt.txt.gz|123531658|2
*/

proc import datafile="F:\pas14\patcpt\phd_20181_patcpt.txt"
	out=CPT.cpt_20181 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 117839336, C = 2 (<2 min) *;

proc import datafile="F:\pas14\patcpt\phd_20182_patcpt.txt"
	out=CPT.cpt_20182 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 2 *;

proc import datafile="F:\pas14\patcpt\phd_20183_patcpt.txt"
	out=CPT.cpt_20183 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 2 *;

proc import datafile="F:\pas14\patcpt\phd_20184_patcpt.txt"
	out=CPT.cpt_20184 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 2 *;

proc import datafile="F:\pas14\patcpt\phd_20191_patcpt.txt"
	out=CPT.cpt_20191 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 2 *;

proc import datafile="F:\pas14\patcpt\phd_20192_patcpt.txt"
	out=CPT.cpt_20192 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 2 *;

proc import datafile="F:\pas14\patcpt\phd_20193_patcpt.txt"
	out=CPT.cpt_20193 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 2 *;

proc import datafile="F:\pas14\patcpt\phd_20194_patcpt.txt"
	out=CPT.cpt_20194 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 2 *;

proc import datafile="F:\pas14\patcpt\phd_20201_patcpt.txt"
	out=CPT.cpt_20201 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 2 *;

proc import datafile="F:\pas14\patcpt\phd_20202_patcpt.txt"
	out=CPT.cpt_20202 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 2 *;

proc import datafile="F:\pas14\patcpt\phd_20203_patcpt.txt"
	out=CPT.cpt_20203 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 2 *;

proc import datafile="F:\pas14\patcpt\phd_20204_patcpt.txt"
	out=CPT.cpt_20204 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 2 *;

proc contents data=CPT.cpt_20181 position;
run;



* ==================================  Step 4. PATICD_DIAG  ================================== *;
/*
PATICD_DIAG|phd_20181_paticd_diag.txt.gz|120640571|5
PATICD_DIAG|phd_20182_paticd_diag.txt.gz|120344025|5
PATICD_DIAG|phd_20183_paticd_diag.txt.gz|119032402|5
PATICD_DIAG|phd_20184_paticd_diag.txt.gz|119574497|5
PATICD_DIAG|phd_20191_paticd_diag.txt.gz|124493089|5
PATICD_DIAG|phd_20192_paticd_diag.txt.gz|127078972|5
PATICD_DIAG|phd_20193_paticd_diag.txt.gz|126650539|5
PATICD_DIAG|phd_20194_paticd_diag.txt.gz|123864947|5
PATICD_DIAG|phd_20201_paticd_diag.txt.gz|114736104|5
PATICD_DIAG|phd_20202_paticd_diag.txt.gz|84425677|5
PATICD_DIAG|phd_20203_paticd_diag.txt.gz|111217261|5
PATICD_DIAG|phd_20204_paticd_diag.txt.gz|110377782|5
*/

proc import datafile="F:\pas14\paticd_diag\phd_20181_paticd_diag.txt"
	out=DIAG.diag_20181 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 120640571, C = 5 (<3 min) *;

proc import datafile="F:\pas14\paticd_diag\phd_20182_paticd_diag.txt"
	out=DIAG.diag_20182 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 5 *;

proc import datafile="F:\pas14\paticd_diag\phd_20183_paticd_diag.txt"
	out=DIAG.diag_20183 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 5 *;

proc import datafile="F:\pas14\paticd_diag\phd_20184_paticd_diag.txt"
	out=DIAG.diag_20184 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 5 *;

proc import datafile="F:\pas14\paticd_diag\phd_20191_paticd_diag.txt"
	out=DIAG.diag_20191 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 5 *;

proc import datafile="F:\pas14\paticd_diag\phd_20192_paticd_diag.txt"
	out=DIAG.diag_20192 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 5 *;

proc import datafile="F:\pas14\paticd_diag\phd_20193_paticd_diag.txt"
	out=DIAG.diag_20193 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 5 *;

proc import datafile="F:\pas14\paticd_diag\phd_20194_paticd_diag.txt"
	out=DIAG.diag_20194 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 5 *;

proc import datafile="F:\pas14\paticd_diag\phd_20201_paticd_diag.txt"
	out=DIAG.diag_20201 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 5 *;

proc import datafile="F:\pas14\paticd_diag\phd_20202_paticd_diag.txt"
	out=DIAG.diag_20202 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 5 *;

proc import datafile="F:\pas14\paticd_diag\phd_20203_paticd_diag.txt"
	out=DIAG.diag_20203 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 5 *;

proc import datafile="F:\pas14\paticd_diag\phd_20204_paticd_diag.txt"
	out=DIAG.diag_20204 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 5 *;

proc contents data=DIAG.icd_diag_20181 position;
run;



* ==================================  Step 5. PATICD_PROC  ================================== *;
/*
PATICD_PROC|phd_20181_paticd_proc.txt.gz|4389557|7
PATICD_PROC|phd_20182_paticd_proc.txt.gz|4391041|7
PATICD_PROC|phd_20183_paticd_proc.txt.gz|4313788|7
PATICD_PROC|phd_20184_paticd_proc.txt.gz|4301303|7
PATICD_PROC|phd_20191_paticd_proc.txt.gz|4298323|7
PATICD_PROC|phd_20192_paticd_proc.txt.gz|4304840|7
PATICD_PROC|phd_20193_paticd_proc.txt.gz|4226183|7
PATICD_PROC|phd_20194_paticd_proc.txt.gz|4119713|7
PATICD_PROC|phd_20201_paticd_proc.txt.gz|3844248|7
PATICD_PROC|phd_20202_paticd_proc.txt.gz|3213054|7
PATICD_PROC|phd_20203_paticd_proc.txt.gz|3712688|7
PATICD_PROC|phd_20204_paticd_proc.txt.gz|3734765|7
*/

proc import datafile="F:\pas14\paticd_proc\phd_20181_paticd_proc.txt"
	out=PROC.proc_20181 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 4389557, C = 7 *;

proc import datafile="F:\pas14\paticd_proc\phd_20182_paticd_proc.txt"
	out=PROC.proc_20182 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 7 *;

proc import datafile="F:\pas14\paticd_proc\phd_20183_paticd_proc.txt"
	out=PROC.proc_20183 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 7 *;

proc import datafile="F:\pas14\paticd_proc\phd_20184_paticd_proc.txt"
	out=PROC.proc_20184 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 7 *;

proc import datafile="F:\pas14\paticd_proc\phd_20191_paticd_proc.txt"
	out=PROC.proc_20191 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 7 *;

proc import datafile="F:\pas14\paticd_proc\phd_20192_paticd_proc.txt"
	out=PROC.proc_20192 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 7 *;

proc import datafile="F:\pas14\paticd_proc\phd_20193_paticd_proc.txt"
	out=PROC.proc_20193 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 7 *;

proc import datafile="F:\pas14\paticd_proc\phd_20194_paticd_proc.txt"
	out=PROC.proc_20194 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 7 *;

proc import datafile="F:\pas14\paticd_proc\phd_20201_paticd_proc.txt"
	out=PROC.proc_20201 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 7 *;

proc import datafile="F:\pas14\paticd_proc\phd_20202_paticd_proc.txt"
	out=PROC.proc_20202 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 7 *;

proc import datafile="F:\pas14\paticd_proc\phd_20203_paticd_proc.txt"
	out=PROC.proc_20203 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 7 *;

proc import datafile="F:\pas14\paticd_proc\phd_20204_paticd_proc.txt"
	out=PROC.proc_20204 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 7 *;

proc contents data=ICD_PROC.icd_proc_20181 position;
run;



* ==================================  Step 6. VITALS  ================================== *;
/*
VITALS|phd_20181_vitals.txt.gz|66377507|15
VITALS|phd_20182_vitals.txt.gz|68571751|15
VITALS|phd_20183_vitals.txt.gz|72771145|15
VITALS|phd_20184_vitals.txt.gz|84285763|15
VITALS|phd_20191_vitals.txt.gz|91327614|15
VITALS|phd_20192_vitals.txt.gz|100731050|15
VITALS|phd_20193_vitals.txt.gz|107923885|15
VITALS|phd_20194_vitals.txt.gz|110436606|15
VITALS|phd_20201_vitals.txt.gz|112167497|15
VITALS|phd_20202_vitals.txt.gz|74810990|15
VITALS|phd_20203_vitals.txt.gz|185217295|15
VITALS|phd_20204_vitals.txt.gz|179782531|15
*/

proc import datafile="F:\pas14\vitals\phd_20181_vitals.txt"
	out=VITALS.vitals_20181 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 66377507, C = 15 (<7 min) *;

proc import datafile="F:\pas14\vitals\phd_20182_vitals.txt"
	out=VITALS.vitals_20182 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 15 *;

proc import datafile="F:\pas14\vitals\phd_20183_vitals.txt"
	out=VITALS.vitals_20183 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 15 *;

proc import datafile="F:\pas14\vitals\phd_20184_vitals.txt"
	out=VITALS.vitals_20184 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 15 *;

proc import datafile="F:\pas14\vitals\phd_20191_vitals.txt"
	out=VITALS.vitals_20191 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 15 *;

proc import datafile="F:\pas14\vitals\phd_20192_vitals.txt"
	out=VITALS.vitals_20192 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 15 *;

proc import datafile="F:\pas14\vitals\phd_20193_vitals.txt"
	out=VITALS.vitals_20193 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 15 *;

proc import datafile="F:\pas14\vitals\phd_20194_vitals.txt"
	out=VITALS.vitals_20194 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 15 *;

proc import datafile="F:\pas14\vitals\phd_20201_vitals.txt"
	out=VITALS.vitals_20201 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 15 *;

proc import datafile="F:\pas14\vitals\phd_20202_vitals.txt"
	out=VITALS.vitals_20202 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 15 *;

proc import datafile="F:\pas14\vitals\phd_20203_vitals.txt"
	out=VITALS.vitals_20203 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 15 *;

proc import datafile="F:\pas14\vitals\phd_20204_vitals.txt"
	out=VITALS.vitals_20204 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 15 *;

proc contents data=VITALS.vitals_20181 position;
run;



* ==================================  Step 7. BILL  ================================== *;
/*
PATBILL|phd_20181_patbill.txt.gz|411010906|10
PATBILL|phd_20182_patbill.txt.gz|395498873|10
PATBILL|phd_20183_patbill.txt.gz|387547645|10
PATBILL|phd_20184_patbill.txt.gz|393563433|10
PATBILL|phd_20191_patbill.txt.gz|410085895|10
PATBILL|phd_20192_patbill.txt.gz|406790779|10
PATBILL|phd_20193_patbill.txt.gz|399042697|10
PATBILL|phd_20194_patbill.txt.gz|394122269|10
PATBILL|phd_20201_patbill.txt.gz|373568815|10
PATBILL|phd_20202_patbill.txt.gz|291167892|10
PATBILL|phd_20203_patbill.txt.gz|352564357|10
PATBILL|phd_20204_patbill.txt.gz|358957682|10
*/

proc import datafile="F:\pas14\patbill\phd_20181_patbill.txt"
	out=BILL.bill_20181 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 411010906, C = 10 (<22 min) *;

proc import datafile="F:\pas14\patbill\phd_20182_patbill.txt"
	out=BILL.bill_20182 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 10 *;

proc import datafile="F:\pas14\patbill\phd_20183_patbill.txt"
	out=BILL.bill_20183 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 10 *;

proc import datafile="F:\pas14\patbill\phd_20184_patbill.txt"
	out=BILL.bill_20184 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 10 *;

proc import datafile="F:\pas14\patbill\phd_20191_patbill.txt"
	out=BILL.bill_20191 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 10 *;

proc import datafile="F:\pas14\patbill\phd_20192_patbill.txt"
	out=BILL.bill_20192 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 10 *;

proc import datafile="F:\pas14\patbill\phd_20193_patbill.txt"
	out=BILL.bill_20193 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 10 *;

proc import datafile="F:\pas14\patbill\phd_20194_patbill.txt"
	out=BILL.bill_20194 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 10 *;

proc import datafile="F:\pas14\patbill\phd_20201_patbill.txt"
	out=BILL.bill_20201 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 10 *;

proc import datafile="F:\pas14\patbill\phd_20202_patbill.txt"
	out=BILL.bill_20202 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 10 *;

proc import datafile="F:\pas14\patbill\phd_20203_patbill.txt"
	out=BILL.bill_20203 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 10 *;

proc import datafile="F:\pas14\patbill\phd_20204_patbill.txt"
	out=BILL.bill_20204 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 10 *;

proc contents data=BILL.bill_20181 position;
run;



* ==================================  Step 8. GENLAB  ================================== *;
/*
GENLAB|phd_20181_genlab.txt.gz|143379414|16
GENLAB|phd_20182_genlab.txt.gz|150574139|16
GENLAB|phd_20183_genlab.txt.gz|153295642|16
GENLAB|phd_20184_genlab.txt.gz|159434123|16
GENLAB|phd_20191_genlab.txt.gz|161838172|16
GENLAB|phd_20192_genlab.txt.gz|168189965|16
GENLAB|phd_20193_genlab.txt.gz|161147090|16
GENLAB|phd_20194_genlab.txt.gz|158844015|16
GENLAB|phd_20201_genlab.txt.gz|156990540|16
GENLAB|phd_20202_genlab.txt.gz|138737049|16
GENLAB|phd_20203_genlab.txt.gz|171164157|16
GENLAB|phd_20204_genlab.txt.gz|167709053|16
*/

proc import datafile="F:\pas14\genlab\phd_20181_genlab.txt"
	out=GENLAB.genlab_20181 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 143379414, C = 16 (<8 min) *;

proc import datafile="F:\pas14\genlab\phd_20182_genlab.txt"
	out=GENLAB.genlab_20182 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 16 *;

proc import datafile="F:\pas14\genlab\phd_20183_genlab.txt"
	out=GENLAB.genlab_20183 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 16 *;

proc import datafile="F:\pas14\genlab\phd_20184_genlab.txt"
	out=GENLAB.genlab_20184 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 16 *;

proc import datafile="F:\pas14\genlab\phd_20191_genlab.txt"
	out=GENLAB.genlab_20191 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 16 *;

proc import datafile="F:\pas14\genlab\phd_20192_genlab.txt"
	out=GENLAB.genlab_20192 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 16 *;

proc import datafile="F:\pas14\genlab\phd_20193_genlab.txt"
	out=GENLAB.genlab_20193 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 16 *;

proc import datafile="F:\pas14\genlab\phd_20194_genlab.txt"
	out=GENLAB.genlab_20194 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 16 *;

proc import datafile="F:\pas14\genlab\phd_20201_genlab.txt"
	out=GENLAB.genlab_20201 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 16 *;

proc import datafile="F:\pas14\genlab\phd_20202_genlab.txt"
	out=GENLAB.genlab_20202 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 16 *;

proc import datafile="F:\pas14\genlab\phd_20203_genlab.txt"
	out=GENLAB.genlab_20203 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 16 *;

proc import datafile="F:\pas14\genlab\phd_20204_genlab.txt"
	out=GENLAB.genlab_20204 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 16 *;

proc contents data=GENLAB.genlab_20181 position;
run;



* ==================================  Step 9. LAB_RES  ================================== *;
/*
LAB_RES|phd_20181_lab_res.txt.gz|5102800|11
LAB_RES|phd_20182_lab_res.txt.gz|4190642|11
LAB_RES|phd_20183_lab_res.txt.gz|4128307|11
LAB_RES|phd_20184_lab_res.txt.gz|4605981|11
LAB_RES|phd_20191_lab_res.txt.gz|5012286|11
LAB_RES|phd_20192_lab_res.txt.gz|4177300|11
LAB_RES|phd_20193_lab_res.txt.gz|3557547|11
LAB_RES|phd_20194_lab_res.txt.gz|3915981|11
LAB_RES|phd_20201_lab_res.txt.gz|4662478|11
LAB_RES|phd_20202_lab_res.txt.gz|3713817|11
LAB_RES|phd_20203_lab_res.txt.gz|4518778|11
LAB_RES|phd_20204_lab_res.txt.gz|5086473|11
*/

proc import datafile="F:\pas14\lab_res\phd_20181_lab_res.txt"
	out=LAB_RES.lab_res_20181 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 5102800, C = 11 *;

proc import datafile="F:\pas14\lab_res\phd_20182_lab_res.txt"
	out=LAB_RES.lab_res_20182 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 11 *;

proc import datafile="F:\pas14\lab_res\phd_20183_lab_res.txt"
	out=LAB_RES.lab_res_20183 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 11 *;

proc import datafile="F:\pas14\lab_res\phd_20184_lab_res.txt"
	out=LAB_RES.lab_res_20184 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 11 *;

proc import datafile="F:\pas14\lab_res\phd_20191_lab_res.txt"
	out=LAB_RES.lab_res_20191 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 11 *;

proc import datafile="F:\pas14\lab_res\phd_20192_lab_res.txt"
	out=LAB_RES.lab_res_20192 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 11 *;

proc import datafile="F:\pas14\lab_res\phd_20193_lab_res.txt"
	out=LAB_RES.lab_res_20193 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 11 *;

proc import datafile="F:\pas14\lab_res\phd_20194_lab_res.txt"
	out=LAB_RES.lab_res_20194 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 11 *;

proc import datafile="F:\pas14\lab_res\phd_20201_lab_res.txt"
	out=LAB_RES.lab_res_20201 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 11 *;

proc import datafile="F:\pas14\lab_res\phd_20202_lab_res.txt"
	out=LAB_RES.lab_res_20202 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 11 *;

proc import datafile="F:\pas14\lab_res\phd_20203_lab_res.txt"
	out=LAB_RES.lab_res_20203 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 11 *;

proc import datafile="F:\pas14\lab_res\phd_20204_lab_res.txt"
	out=LAB_RES.lab_res_20204 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 11 *;

proc contents data=LAB_RES.lab_res_20181 position;
run;



* ==================================  Step 10. LAB_SENS  ================================== *;
/*
LAB_SENS|phd_20181_lab_sens.txt.gz|2344831|12
LAB_SENS|phd_20182_lab_sens.txt.gz|2418868|12
LAB_SENS|phd_20183_lab_sens.txt.gz|2640012|12
LAB_SENS|phd_20184_lab_sens.txt.gz|2526247|12
LAB_SENS|phd_20191_lab_sens.txt.gz|2354360|12
LAB_SENS|phd_20192_lab_sens.txt.gz|2354010|12
LAB_SENS|phd_20193_lab_sens.txt.gz|2320176|12
LAB_SENS|phd_20194_lab_sens.txt.gz|2121786|12
LAB_SENS|phd_20201_lab_sens.txt.gz|2009777|12
LAB_SENS|phd_20202_lab_sens.txt.gz|1743066|12
LAB_SENS|phd_20203_lab_sens.txt.gz|2190629|12
LAB_SENS|phd_20204_lab_sens.txt.gz|2022727|12
*/

proc import datafile="F:\pas14\lab_sens\phd_20181_lab_sens.txt"
	out=LAB_SENS.lab_sens_20181 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 2344831, C = 12 *;

proc import datafile="F:\pas14\lab_sens\phd_20182_lab_sens.txt"
	out=LAB_SENS.lab_sens_20182 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 12 *;

proc import datafile="F:\pas14\lab_sens\phd_20183_lab_sens.txt"
	out=LAB_SENS.lab_sens_20183 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 12 *;

proc import datafile="F:\pas14\lab_sens\phd_20184_lab_sens.txt"
	out=LAB_SENS.lab_sens_20184 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 12 *;

proc import datafile="F:\pas14\lab_sens\phd_20191_lab_sens.txt"
	out=LAB_SENS.lab_sens_20191 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 12 *;

proc import datafile="F:\pas14\lab_sens\phd_20192_lab_sens.txt"
	out=LAB_SENS.lab_sens_20192 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 12 *;

proc import datafile="F:\pas14\lab_sens\phd_20193_lab_sens.txt"
	out=LAB_SENS.lab_sens_20193 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 12 *;

proc import datafile="F:\pas14\lab_sens\phd_20194_lab_sens.txt"
	out=LAB_SENS.lab_sens_20194 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 12 *;

proc import datafile="F:\pas14\lab_sens\phd_20201_lab_sens.txt"
	out=LAB_SENS.lab_sens_20201 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 12 *;

proc import datafile="F:\pas14\lab_sens\phd_20202_lab_sens.txt"
	out=LAB_SENS.lab_sens_20202 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 12 *;

proc import datafile="F:\pas14\lab_sens\phd_20203_lab_sens.txt"
	out=LAB_SENS.lab_sens_20203 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 12 *;

proc import datafile="F:\pas14\lab_sens\phd_20204_lab_sens.txt"
	out=LAB_SENS.lab_sens_20204 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 12 *;

proc contents data=LAB_SENS.lab_sens_20181 position;
run;



* ==================================  Step 11. READMIT  ================================== *;
/*
READMIT|phd_20181_readmit.txt.gz|23434819|23
READMIT|phd_20182_readmit.txt.gz|23176219|23
READMIT|phd_20183_readmit.txt.gz|22720107|23
READMIT|phd_20184_readmit.txt.gz|22775654|23
READMIT|phd_20191_readmit.txt.gz|23382219|23
READMIT|phd_20192_readmit.txt.gz|23777726|23
READMIT|phd_20193_readmit.txt.gz|23716112|23
READMIT|phd_20194_readmit.txt.gz|23193102|23
READMIT|phd_20201_readmit.txt.gz|21147007|23
READMIT|phd_20202_readmit.txt.gz|14894131|23
READMIT|phd_20203_readmit.txt.gz|20351916|23
READMIT|phd_20204_readmit.txt.gz|20332455|23
*/

proc import datafile="F:\pas14\readmit\phd_20181_readmit.txt"
	out=READMIT.readmit_20181 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = 23434819, C = 23 (<2 min) *;

proc import datafile="F:\pas14\readmit\phd_20182_readmit.txt"
	out=READMIT.readmit_20182 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 23 *;

proc import datafile="F:\pas14\readmit\phd_20183_readmit.txt"
	out=READMIT.readmit_20183 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 23 *;

proc import datafile="F:\pas14\readmit\phd_20184_readmit.txt"
	out=READMIT.readmit_20184 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 23 *;

proc import datafile="F:\pas14\readmit\phd_20191_readmit.txt"
	out=READMIT.readmit_20191 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 23 *;

proc import datafile="F:\pas14\readmit\phd_20192_readmit.txt"
	out=READMIT.readmit_20192 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 23 *;

proc import datafile="F:\pas14\readmit\phd_20193_readmit.txt"
	out=READMIT.readmit_20193 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 23 *;

proc import datafile="F:\pas14\readmit\phd_20194_readmit.txt"
	out=READMIT.readmit_20194 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 23 *;

proc import datafile="F:\pas14\readmit\phd_20201_readmit.txt"
	out=READMIT.readmit_20201 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 23 *;

proc import datafile="F:\pas14\readmit\phd_20202_readmit.txt"
	out=READMIT.readmit_20202 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 23 *;

proc import datafile="F:\pas14\readmit\phd_20203_readmit.txt"
	out=READMIT.readmit_20203 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 23 *;

proc import datafile="F:\pas14\readmit\phd_20204_readmit.txt"
	out=READMIT.readmit_20204 (compress=char)
	dbms=dlm replace;
	delimiter='|';
   	guessingrows=16550;
run;
* R = XXX, C = 23 *;

proc contents data=READMIT.readmit_20181 position;
run;

**************************************************  End  **************************************************;
