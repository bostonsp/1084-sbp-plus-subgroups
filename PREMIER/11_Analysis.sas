﻿/*---------------------------------------------------------------------------------
Project:            1084

Program:            ____

Function:           ____

Dependencies:		____

Dependents:			____

Author:             ____

Date Completed:     ____

SAS Version:        ____   

Platform:           ____

Macros Called:      ____ 

Macro Parameters:   ____

Comments:           ____

Example:			____

Revisions:
date	name	what
211101	SH		Initial Version
---------------------------------------------------------------------------------*/

* ==================================  Step 1. XXX  ================================== *;
proc means data=VITALS.vitals_20181 n nmiss min max;
run;

proc sql;
	select  n(distinct ORDERING_PROVIDER) as R
	from VITALS.vitals_20181;
quit;
* R = 461 *;


proc sql;
	create table zzz1 as
	select distinct PROV_ID
	from DEMO.demo_20181
	
	union corr
	select distinct PROV_ID
	from DEMO.demo_20182

	union corr
	select distinct PROV_ID
	from DEMO.demo_20183

	union corr
	select distinct PROV_ID
	from DEMO.demo_20184

	union corr
	select distinct PROV_ID
	from DEMO.demo_20191
	
	union corr
	select distinct PROV_ID
	from DEMO.demo_20192

	union corr
	select distinct PROV_ID
	from DEMO.demo_20193

	union corr
	select distinct PROV_ID
	from DEMO.demo_20194

	union corr
	select distinct PROV_ID
	from DEMO.demo_20201
	
	union corr
	select distinct PROV_ID
	from DEMO.demo_20202

	union corr
	select distinct PROV_ID
	from DEMO.demo_20203

	union corr
	select distinct PROV_ID
	from DEMO.demo_20204

	order by PROV_ID;
quit;
* R = 911 (<11 min) *;

proc sql;
	select  n(distinct PROV_ID) as R
	from zzz1;
quit;
* R = 911 *;
























**************************************************  End  **************************************************;
