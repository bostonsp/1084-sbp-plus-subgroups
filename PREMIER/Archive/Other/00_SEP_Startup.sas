﻿/*---------------------------------------------------------------------------------
Project:            0867 Grifols Sepsis and Cirrhosis in Premier

Program:            ____

Function:           ____

Dependencies:		____

Dependents:			____

Author:             ____

Date Completed:     ____

SAS Version:        ____   

Platform:           ____

Macros Called:      ____ 

Macro Parameters:   ____

Comments:           ____

Example:			____

Revisions:
date	name	what
191119	XM		Initial Version
191121	XM		Updated after data has been segmented
191209	XM		Updated to point to the new data set
---------------------------------------------------------------------------------*/

/*Data for the patient key and other demographic information*/
libname NOAPR V9 "S:\0867_PREMIER\201911\NOAPR\" ACCESS = READONLY; 

/*Diagnosis data*/
libname DIAG V9 "S:\0867_PREMIER\201911\DIAG\" ACCESS = READONLY;

/*Procedure data*/
libname PROC V9 "S:\0867_PREMIER\201911\PROC\" ACCESS = READONLY;

/*CPT data */
libname CPT V9 "S:\0867_PREMIER\201911\CPT\" ACCESS = READONLY;

/*Lab data*/
libname LAB V9 "S:\0867_PREMIER\pas24\" ACCESS = READONLY;

/*READMIT data*/
libname READMIT V9 "S:\0867_PREMIER\201911\READMIT\" ACCESS = READONLY;

/*Definition tables*/
libname DEF V9 "S:\0867_PREMIER\201911\DEF\" ACCESS = READONLY;

/*Billing tables*/
libname BILL V9 "S:\0867_PREMIER\201911\BILL\" ACCESS = READONLY;

/*Local data*/
LIBNAME SEP "F:\0867\Sepsis\Data";

/*Format*/
libname FMT0867 V9 "F:\0867\FMT0867" ACCESS=READONLY;
options fmtsearch=(FMT0867 WORK LIBRARY);

/*Macro*/
%include "F:\SH\gethffdata.sas";

**************************************************  End  **************************************************;
