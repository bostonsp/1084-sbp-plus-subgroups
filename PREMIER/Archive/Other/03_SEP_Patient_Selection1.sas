﻿/*---------------------------------------------------------------------------------
Project:            0867 Grifols Sepsis and Cirrhosis in Premier

Program:            ____ 

Function:           Perform the patient selection

Dependencies:		SEP.ALL_SELECTION_FLAGS

Dependents:			____

Author:             ____

Date Completed:     ____

SAS Version:        ____   

Platform:           ____

Macros Called:      ____ 

Macro Parameters:   ____

Comments:           ____

Example:			____

Revisions:
date	name	what
200214	XM		Updated to remove the corrupted visit
200129	XM		Update to look at the LOS based on p1 and p99 instead of 48 hrs 	
				and 90th percentile
200124	DKH		EXCLUDE visits with PAT_COST LT P1 or GT P99
200120	XM		Initial version
---------------------------------------------------------------------------------*/

* ==================================  Step 1. Inclusion 1: Sepsis  ================================== *;
/*Start by creating a macro for count the patients and visits at each step*/
%macro count_pat_vis(dsin = );
	
	proc sql;
		select 	count(distinct medrec_key) as P format=10.,
				count(distinct pat_key) as V format=10.,
				count(*) as R format=10.
		from &dsin.;
	quit;

%mend count_pat_vis;

%count_pat_vis(dsin = SEP.Sepsis_NOAPR);
* P = 778688, V = R = 866222 *;



* ==================================  Step 2. Inclusion 2: Admission between Jan 2016 and Jun 2019  ================================== *;
data attrition1A;
	set SEP.Sepsis_NOAPR;
	year = substr(put(ADM_MON, 8.), 1, 5);
run;
* R = 866222 *;

proc freq data=attrition1A;
	*table year / missing norow nocol;
	table ADM_MON * year / missing norow nocol nocum nopercent;
run;
/*
2008 1 0.00 1 0.00 
2014 11 0.00 12 0.00 
2015 7277 0.84 7289 0.84 
2016 246558 28.46 253847 29.31 
2017 253525 29.27 507372 58.57 
2018 254009 29.32 761381 87.90 
2019 104841 12.10 866222 100.00
*/

data attrition1B;
	set attrition1A;
	if 2016 <= year <= 2019;
run;
%count_pat_vis(dsin = attrition1B);
* P = 772326, V = R = 858933 *;



* ==================================  Step 3. Inclusion 3: Patient ≥ 18 y.o.  ================================== *;
data attrition2;
	set attrition1B;
	if age >= 18;
run;
%count_pat_vis(dsin = attrition2);
* P = 766656, V = R = 852977 *;



* ==================================  Step 4. Inclusion 4: Inpatient encounter status  ================================== *;
data attrition3;
	set attrition2;
	if I_O_IND = "I";
run;
%count_pat_vis(dsin = attrition3);
* P = 748583, V = R = 827594 *;



* ==================================  Step 5. Exclusion 1: Patient admitted through a hospital facility transfer  ================================== *;
proc freq data=attrition3;
	table ADM_SOURCE / missing norow nocol;
run; 
/*
1 648732 78.39 648732 78.39 
2 37387 4.52 686119 82.91 
4 79964 9.66 766083 92.57	=> Transfer from a hospital (different facility) 
5 38747 4.68 804830 97.25 
6 11384 1.38 816214 98.62 
8 570 0.07 816784 98.69 
9 5019 0.61 821803 99.30 
B 3 0.00 821806 99.30 
C 1 0.00 821807 99.30 
D 4761 0.58 826568 99.88 
E 223 0.03 826791 99.90 
F 803 0.10 827594 100.00 
*/

data attrition4;
	set attrition3;
	if ADM_SOURCE ne "4";
run; 
%count_pat_vis(dsin = attrition4);
* P = 675870, V = R = 747630 *;



* ==================================  Step 6. Exclusion 2: No large volume crystalloids AND no albumin  ================================== *;
/*Select the albumin ids*/
proc sql noprint;
	select distinct quote(std_chg_code)
	into :albids separated by ","
	from DEF.DEF_CHGMST
	where lowcase(prod_name_desc) like "%%albumin%";
quit; 

/*Start to outline code for selecting the ids into a variable*/
/*Set up table to create lvpids*/
proc sql noprint;
	create table lvp1 as 
	select distinct * 
	from DEF.DEF_CHGMST
	where lowcase(clin_sum_desc) like "%%hospital solutions iv parenteral%";
quit; 

/*Create new variables in order to select lvpids*/
data lvp2;
	set lvp1;
	num_words = countw(clin_dtl_desc);
	last_word = scan(clin_dtl_desc, num_words);
run; 
* R = 255 *;

/*Check the new variables created*/
proc freq data = lvp2;
	tables last_word num_words / missing norow nocol;
run; 

/* Select the lvpids*/
/*Update on 200320 for greater than 250ML and not just greater than or equal to 250 ML*/
proc sql noprint;
	select distinct quote(std_chg_code)
	into :lvpids separated by ","
	from lvp2
	where last_word in (/*"250ML",*/ "500ML", "1000ML");
quit;

/*Pull these items from the billing data*/
proc sql;
	create table alb_lvp as
	select 	distinct medrec_key,
			pat_key
	from SEP.Sepsis_BILL
	where std_chg_code in (&albids., &lvpids.)
	order by medrec_key, pat_key;
quit;
* R = 755287 *;

proc sql;
	create table attrition5 as
	select t1.*
	from attrition4 as t1 inner join alb_lvp as t2 on t1.medrec_key = t2.medrec_key and t1.pat_key = t2.pat_key
	order by t1.medrec_key, t1.pat_key;
quit;
%count_pat_vis(dsin = attrition5);
* P = 601769, V = R = 657966 *;

* ==================================  Step 7. Exclusion 3: No antibiotics  ================================== *;
/*Select the antibiotics*/
proc sql noprint;
	select distinct quote(std_chg_code)
	into :abxids separated by ","
	from DEF.DEF_CHGMST
	where prod_class_desc in (
		'AMEBACIDES',
		'AMINOGLYCOSIDES',
		'ANTI-FUNGAL, MISC',
		'ANTI-INFECTIVE, NON-SYSTEMIC',
		'ANTI-INFECTIVES SYSTEMIC MISC',
		'ANTIMALARIALS',
		'ANTISEPTICS, GENERAL',
		'ANTISEPTICS, MOUTH/THROAT',
		'ANTISEPTICS, THERAPEUTIC',
		'ANTIVIRALS, HIV',
		'ANTIVIRALS, OTHER',
		'BETA-LACTAM',
		'CEPHALOSPORINS & RELATED',
		'CHLORAMPHENICOL',
		'CYTOSTATIC ANTIBIOTICS',
		'GLYCYLCYCLINES',
		'MACROLIDES & RELATED',
		'OPHTH ANTI-INFECTIVE',
		'OTIC ANTI-INFECTIVE',
		'PENICILLINS',
		'QUINOLONES',
		'STREPTOMYCIN',
		'SULFAS & TRIMETHOPRIM',
		'TETRACYCLINES',
		'TRICHOM/ANTIBACTERIALS',
		'TUBERCULOSIS THERAPY',
		'UT ANTI-INFECTIVES');
quit; 

/*Pull these items from the billing data*/
proc sql;
	create table abx as
	select 	distinct medrec_key,
			pat_key
	from SEP.Sepsis_BILL
	where std_chg_code in (&abxids.)
	order by medrec_key, pat_key;
quit;
* R = 844559 *;

proc sql;
	create table attrition6 as
	select t1.*
	from attrition5 as t1 inner join abx as t2 on t1.medrec_key = t2.medrec_key and t1.pat_key = t2.pat_key
	order by t1.medrec_key, t1.pat_key;
quit;
%count_pat_vis(dsin = attrition6);
* P = 599331, V = R = 654969 *; 


  
* ==================================  Step 8. Exclusion 4: Gender not specified  ================================== *;
proc freq data=attrition6;
	table GENDER / missing norow nocol;
run;  
/*
F 319449 48.77 319449 48.77 
M 335497 51.22 654946 100.00 
U 23 0.00 654969 100.00 
*/

data attrition7;
	set attrition6;
	if GENDER in ("F", "M");
run;
%count_pat_vis(dsin = attrition7);
* P = 599309, V = R = 654946 *;

  

* ==================================  Step 9. Exclusion 5: Bed Size not specified  ================================== *;
data attrition8A;
	set attrition7;
	bed_size_group = put(prov_id, BSRFMT.);
run;
* R = 654946 *;

proc freq data=attrition8A;
	table bed_size_group / missing norow nocol;
run;
/*
000-099 33186 5.07 33186 5.07 
100-199 99553 15.20 132739 20.27 
200-299 109381 16.70 242120 36.97 
300-399 116018 17.71 358138 54.68 
400-499 78089 11.92 436227 66.61 
500+ 218719 33.39 654946 100.00 
*/

data attrition8B;
	set attrition8A;
	if bed_size_group ne "Unavailable";
run;
%count_pat_vis(dsin = attrition8B);
* P = 599309, V = R = 654946 *;



* ==================================  Step 10. Exclusion 6: Admission type except 'Urgent' or 'Emergent'  ================================== *;
data attrition9A;
	set attrition8B;
	admit_type_desc = put(adm_type, ATYPFMT.);
run;
* R = 654946 *;

proc freq data=attrition9A;
	table admit_type_desc / missing norow nocol;
run;
/*
ELECTIVE 21902 3.34 21902 3.34 
EMERGENCY 580577 88.65 602479 91.99 
INFORMATION NOT AVAILABLE 3446 0.53 605925 92.52 
TRAUMA CENTER 2912 0.44 608837 92.96 
URGENT 46109 7.04 654946 100.00 
*/

data attrition9B;
	set attrition9A;
	if admit_type_desc in ("EMERGENCY", "URGENT");
run;
%count_pat_vis(dsin = attrition9B);
* P = 573968, V = R = 626686 *;
	   
data SEP.attrition9B;
	set attrition9B;
run;

**************************************************  End  **************************************************;
