﻿/*---------------------------------------------------------------------------------
Project:            0867 Grifols Sepsis and Cirrhosis in Premier

Program:            ____ 

Function:           Start by pulling all of the dx information for the 
					high cut of sepsis patients and encounters

Dependencies:		____

Dependents:			____

Author:             ____

Date Completed:     ____

SAS Version:        ____   

Platform:           ____

Macros Called:      ____ 

Macro Parameters:   ____

Comments:           ____

Example:			____

Revisions:
date	name	what
191124	XM		Initial Version
200116	XM		Updated for the new dataset and for 256 segments
---------------------------------------------------------------------------------*/

/*With the large cohort of patients before the selection, pull the DX tables*/
%macro loop_dx (dsin = , start_seg = , end_seg = );
%local i;
	
	/*Start by selecting visit data for patients of interest*/
	%do i = &start_seg. %to &end_seg.;
		proc sql;
			create view dx_&i. as
			select distinct t2.*
			from &dsin. as t1 inner join DIAG.DIAG_&i. as t2 on
				t1.medrec_key = t2.medrec_key and t1.pat_key = t2.pat_key
			order by t1.medrec_key, t1.pat_key;
		quit; 
	%end;
	
	/*Add the datasets together*/
	data diag1 / view = diag1;
		set dx_&start_seg. - dx_&end_seg.;
	run; 

	proc sort data = diag1 out = diag2 presorted noduprec;
		by medrec_key pat_key;
	run;

	%do i = &start_seg. %to &end_seg.;
		/*Delete any datasets have same name as views later*/
		proc datasets lib = work nolist;
			delete dx_&i. (memtype = view) diag1 (memtype = view);
		quit;
		run;
	%end;

%mend loop_dx; 

%loop_dx(dsin = SEP.Sepsis_NOAPR, start_seg = 0, end_seg = 255);
* R = 21520156 (<8 min) *;

/*Save a copy of this data*/
proc sort data = diag2 out = SEP.Sepsis_DIAG presorted noduprec;
	by medrec_key pat_key;
run; 
* R = 21520156 *;

proc sql;
	select 	count(distinct medrec_key) as P format=10.,
			count(distinct pat_key) as V format=10.
	from SEP.Sepsis_DIAG;
quit;
* P = 778688, V = 866222 *;

**************************************************  End  **************************************************;
