﻿/*-----------------------------------------------------------------------------
Project:            ____

Program:            0867_LA_01_Labs_Import.sas 

Function:           ____

Dependencies:		____

Dependents:			____

Outputs:			____

Author:             ____

SAS Version:        ____   

Comments:           ____

Revisions:
date	name	what
200629	DKH		Initial Version
-----------------------------------------------------------------------------*/
libname MEM clear;
libname MEM V9 "E:\david\memlib\" MEMLIB;

%macro mac;
%local i j;

%do i=2016 %to 2019;
	%do j=1 %to 4;

		%if (&i. EQ 2019) AND (&j. GE 3) %then %do;
			%put NOTE: skipping;
		%end;
		%else %do;
			filename rp&i.&j. "G:\pas24\grifols_&i.&j._genlab.txt" lrecl=16550;

			proc import
				file=rp&i.&j.
				out=mem.lab&i.&j. (compress=char)
				dbms=dlm
				replace
			;
			delimiter = "|";
			guessingrows=16550;
			run;
			%if &syserr. GT 4 %then %abort;
		%end;
	%end;
%end;
%mend mac;
%mac;

