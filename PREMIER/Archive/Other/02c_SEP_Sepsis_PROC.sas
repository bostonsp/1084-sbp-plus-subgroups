﻿/*---------------------------------------------------------------------------------
Project:            0867 Grifols Sepsis and Cirrhosis in Premier

Program:            ____ 

Function:           Start by pulling all of the proc information for the 
					high cut of cirrhosis patients and encounters

Dependencies:		____

Dependents:			____

Author:             ____

Date Completed:     ____

SAS Version:        ____   

Platform:           ____

Macros Called:      ____ 

Macro Parameters:   ____

Comments:           ____

Example:			____

Revisions:
date	name	what
191124	XM		Initial Version
200116	XM		Updated for the new data set and 256 segments
---------------------------------------------------------------------------------*/

/*With the large cohort of patients before the selection, pull the PR tables*/
%macro loop_proc (dsin = , start_seg = , end_seg = );
%local i;
	
	/*Start by selecting visit data for patients of interest*/
	%do i = &start_seg. %to &end_seg.;
		proc sql;
			create view proc_&i. as
			select distinct t2.*
			from &dsin. as t1 inner join PROC.PROC_&i. as t2 on
				t1.medrec_key = t2.medrec_key and t1.pat_key = t2.pat_key
			where t2.PROC_DAY >= 0
			order by t1.medrec_key, t1.pat_key;
		quit; 
	%end;
	
	/*Add the datasets together*/
	data proc1 / view = proc1;
		set proc_&start_seg. - proc_&end_seg.;
	run; 

	proc sort data = proc1 out = proc2 presorted noduprec;
		by medrec_key pat_key;
	run;

	%do i = &start_seg. %to &end_seg.;
		/*Delete any datasets have same name as views later*/
		proc datasets lib = work nolist;
			delete proc_&i. (memtype = view) proc1 (memtype = view);
		quit;
		run;
	%end;

%mend loop_proc; 

%loop_proc(dsin = SEP.Sepsis_NOAPR, start_seg = 0, end_seg = 255);
* R = 3152639 (<2 min) *;

/*Save this data*/
proc sort data = proc2 out = SEP.Sepsis_PROC presorted;
	by medrec_key pat_key;
run;
* R = 3152639 *;

proc sql;
	select 	count(distinct medrec_key) as P format=10.,
			count(distinct pat_key) as V format=10.
	from SEP.Sepsis_PROC;
quit;
* P = 571911, V = 619134 *;

**************************************************  End  **************************************************;
