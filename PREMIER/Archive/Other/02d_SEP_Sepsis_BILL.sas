﻿/*---------------------------------------------------------------------------------
Project:            0867 Grifols Sepsis and Cirrhosis in Premier

Program:            ____ 

Function:           Start by pulling all of the billing information for the 
					high cut of sepsis patients and encounters

Dependencies:		____

Dependents:			____

Author:             ____

Date Completed:     ____

SAS Version:        ____   

Platform:           ____

Macros Called:      ____ 

Macro Parameters:   ____

Comments:           ____

Example:			____

Revisions:
date	name	what
191124	XM		Initial Version
200116	XM		Updated for teh new cut of data
---------------------------------------------------------------------------------*/

/*With the large cohort of patients before the selection, pull the billing tables*/
%macro loop_bill (dsin = , start_seg = , end_seg = );
%local i;
	
	/*Start by selecting visit data for patients of interest*/
	%do i = &start_seg. %to &end_seg.;
		proc sql;
			create view bill_&i. as
			select distinct t2.*
			from &dsin. as t1 inner join BILL.BILL_&i. as t2 on
				t1.medrec_key = t2.medrec_key and t1.pat_key = t2.pat_key
			where t2.SERV_DAY >= 0
			order by t1.medrec_key, t1.pat_key;
		quit; 
	%end;
	
	/*Add the datasets together*/
	data bill1 / view = bill1;
		set bill_&start_seg. - bill_&end_seg.;
	run; 

	proc sort data = bill1 out = bill2 presorted noduprec;
		by medrec_key pat_key;
	run;

	%do i = &start_seg. %to &end_seg.;
		/*Delete any datasets have same name as views later*/
		proc datasets lib = work nolist;
			delete bill_&i. (memtype = view) bill1 (memtype = view);
		quit;
		run;
	%end;

%mend loop_bill; 

%loop_bill (dsin = SEP.Sepsis_NOAPR, start_seg = 0, end_seg = 255);
* R = 266141650 (<50 min) *;

/*Save the dataset*/
proc sort data = bill2 out = SEP.Sepsis_BILL presorted noduprec;
	by medrec_key pat_key;
run; 
* R = 266141650 *;

proc sql;
	select 	count(distinct medrec_key) as P format=10.,
			count(distinct pat_key) as V format=10.
	from SEP.Sepsis_BILL;
quit;
* P = 778687, V = 866220 *;

**************************************************  End  **************************************************;

