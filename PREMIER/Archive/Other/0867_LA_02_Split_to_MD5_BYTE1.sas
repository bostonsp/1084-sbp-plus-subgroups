﻿/*-----------------------------------------------------------------------------
Project:            ____

Program:            0867_LA_02_Split_to_MD5_BYTE1.sas

Function:           ____

Dependencies:		GR_YYYYM_PATICD_DIAG.SAS7BDAT
					key table PREM.MEDREC_KEYTABLE

Dependents:			____

Outputs:			____

Author:             ____

SAS Version:        ____   

Comments:           ____

Revisions:
date	name	what
200626	DKH		Adapt to process lab tbls
191205	XM		Updated to use libnames for PAS23/201911 for the new data 
				that was sent over
191120	DKH		First version
-----------------------------------------------------------------------------*/
libname ORIG V9 "E:\david\tmp" access=readonly;
libname LAB V9 "S:\0867_PREMIER\pas24\";

/*	select memname metadata into global macro variable for macro later on	*/
PROC SQL NOPRINT;
	SELECT
		DISTINCT MEMNAME into :mnames SEPARATED BY ' '
	FROM
		SASHELP.VCOLUMN
	WHERE
		STRIP(LIBNAME) EQ 'ORIG'
		AND UPCASE(STRIP(MEMNAME)) CONTAINS 'LAB'
	;
QUIT;
%put &mnames.;

/*	Create a list of the largest size of the variables for manual type-in */
PROC SQL;
	CREATE TABLE _conts AS
	SELECT
		LIBNAME
		,MEMNAME
		,NAME
		,LENGTH
		,FORMAT
	FROM
		SASHELP.VCOLUMN
	WHERE
		STRIP(LIBNAME) EQ 'ORIG'
		AND UPCASE(STRIP(MEMNAME)) CONTAINS 'LAB'
	ORDER BY
		NAME ASC
		,LENGTH DESC
	;
QUIT;

data _conts2;
	set _conts;
	by NAME descending LENGTH;
	if first.NAME then output;
run;

proc print data = _conts2;
run;

/*	fix 20163 result_time_of_day */
data mem.LAB20163(compress=char);
set mem.LAB20163(rename=(RESULT_TIME_OF_DAY=_rtod) obs=max);
length RESULT_TIME_OF_DAY 8;
format RESULT_TIME_OF_DAY TIME20.3;
RESULT_TIME_OF_DAY = input(_rtod,TIME20.3);
run;

/*	fix COLLECTION_TIME_OF_DAY */
%macro loop;
%local tables i thistbl;
%let tables = LAB20173 LAB20183 LAB20164 LAB20161 LAB20162 LAB20181 LAB20172 LAB20184;
%do i=1 %to 8;
%let thistbl = %sysfunc(scan(%superq(tables),&i.));
%put NOTE: &thistbl.;
data mem.&thistbl. (compress=char);
set mem.&thistbl.(rename=(COLLECTION_TIME_OF_DAY=_ctod));
length COLLECTION_TIME_OF_DAY 8;
format COLLECTION_TIME_OF_DAY TIME20.3;
COLLECTION_TIME_OF_DAY = input(_ctod,TIME20.3);
run;
%end;
%mend loop;
%loop;

libname tmp V9 "E:\david\tmp";
%macro mac;
%local i j;

%do i=2016 %to 2019;
	%do j=1 %to 4;

		%if (&i. EQ 2019) AND (&j. GE 3) %then %do;
			%put NOTE: skipping;
		%end;
		%else %do;
			data tmp.lab&i.&j. (compress=no);
			set mem.lab&i.&j.;
			run;
			%if &syserr. GT 4 %then %abort;
		%end;
	%end;
%end;
%mend mac;
%mac;

/*	NOT USED - BEGIN 	*/
%macro mac_diag;
%global &mnames;
%local i memcount thismem j;

%let memcount=%sysfunc(countw(%superq(mnames)));
%put NOTE: &memcount.;
data
	%do j=0 %to 255;
		LAB.LAB_%unquote(&j.) (compress=char)
	%end;
;

	length
		MD5_BYTE1 3
		PAT_KEY 8
		ORDER_KEY 8
		SPECIMEN_SOURCE_SNOMED_CODE $ 6
		SPECIMEN_SOURCE_SNOMED_DESC $ 58
		COLLECTION_DAY_NUMBER 8
		COLLECTION_TIME_OF_DAY 8
		LAB_TEST_LOINC_CODE $ 10
		LAB_TEST_LOINC_DESC $ 128 /* $ 134 */
		LAB_TEST_RESULT $ 96 /*$ 220*/
		RESULT_UNIT $ 17
		FLAGS $ 2
		RESULT_DAY_NUMBER 8
		RESULT_TIME_OF_DAY 8
		REFERENCE_INTERVAL $ 16 /*$ 126*/
		NUMERIC_VALUE 8
		NUMERIC_VALUE_OPERATOR $ 2
	;

	format
		MD5_BYTE1 BEST3.
		PAT_KEY BEST12.
		ORDER_KEY BEST12.
		SPECIMEN_SOURCE_SNOMED_CODE $6.
		SPECIMEN_SOURCE_SNOMED_DESC $58.
		COLLECTION_DAY_NUMBER BEST12.
		COLLECTION_TIME_OF_DAY TIME20.3
		LAB_TEST_LOINC_CODE $10.
		LAB_TEST_LOINC_DESC $128. /*$134.*/
		LAB_TEST_RESULT $96. /*$220.*/
		RESULT_UNIT $17.
		FLAGS $2.
		RESULT_DAY_NUMBER BEST12.
		RESULT_TIME_OF_DAY TIME20.3
		REFERENCE_INTERVAL $16. /*$126.*/
		NUMERIC_VALUE BEST12.
		NUMERIC_VALUE_OPERATOR $2.
	;

	if (_N_ EQ 1) then do;

		/*	declare fact table	*/
		declare hash hf(ordered:'A',hashexp:10);
		declare hiter hfi('hf');
		rc1=hf.defineKey(
			'MD5_BYTE1'
			,'PAT_KEY'
			,'ORDER_KEY'
			,'SPECIMEN_SOURCE_SNOMED_CODE'
			,'SPECIMEN_SOURCE_SNOMED_DESC'
			,'COLLECTION_DAY_NUMBER'
			,'COLLECTION_TIME_OF_DAY'
			,'LAB_TEST_LOINC_CODE'
			,'LAB_TEST_LOINC_DESC'
			,'LAB_TEST_RESULT'
			,'RESULT_UNIT'
			,'FLAGS'
			,'RESULT_DAY_NUMBER'
			,'RESULT_TIME_OF_DAY'
			,'REFERENCE_INTERVAL'
			,'NUMERIC_VALUE'
			,'NUMERIC_VALUE_OPERATOR'
		);
		rc1=hf.defineData(
			'MD5_BYTE1'
			,'PAT_KEY'
			,'ORDER_KEY'
			,'SPECIMEN_SOURCE_SNOMED_CODE'
			,'SPECIMEN_SOURCE_SNOMED_DESC'
			,'COLLECTION_DAY_NUMBER'
			,'COLLECTION_TIME_OF_DAY'
			,'LAB_TEST_LOINC_CODE'
			,'LAB_TEST_LOINC_DESC'
			,'LAB_TEST_RESULT'
			,'RESULT_UNIT'
			,'FLAGS'
			,'RESULT_DAY_NUMBER'
			,'RESULT_TIME_OF_DAY'
			,'REFERENCE_INTERVAL'
			,'NUMERIC_VALUE'
			,'NUMERIC_VALUE_OPERATOR'
		);
		rc1=hf.defineDone();

	end;

	set
		%do i=1 %to &memcount.;
		%let thismem = %sysfunc(scan(%superq(mnames),%unquote(&i.)));
			ORIG.%unquote(&thismem.)(obs=max)
		%end;
		END=DONE
	;

	MD5_BYTE1 = rank (char (md5 (strip (PAT_KEY)), 1));

	if (0 <= MD5_BYTE1 <= 255)
	then rc1=hf.replace();

	if (DONE) then do;
		/*rc1=hf.output(dataset:"work.DIAG(compress=no)");*/
		rc1=hfi.first();
		do while (rc1 EQ 0);
			%do j=0 %to 255;
				%if &j. GT 0 %then %do;
				else
				%end;
				if (MD5_BYTE1 EQ &j.) then output LAB.LAB_%unquote(&j.);
			%end;
			rc1=hfi.next();
		end;
	end;

	drop
		rc1
		_ctod
		_rtod
	;
run;
%if &syserr. GT 4 %then %abort;
%mend mac_diag;
%*mac_diag;


%macro setmeta;
%local j;
%do j=0 %to 255;
	proc sort data=LAB.LAB_%unquote(&j.)(compress=char) nodupkey presorted;
	by _ALL_
	;
	run;
	%if &syserr. GT 4 %then %abort;
%end;
%mend setmeta;
%*setmeta;
/*	NOT USED - END 	*/


%macro mac_diag_v2;
%global &mnames;
%local i memcount thismem j;

%let memcount=%sysfunc(countw(%superq(mnames)));
%put NOTE: &memcount.;
data
	%do j=0 %to 255;
		LAB.LAB_%unquote(&j.) (compress=char)
	%end;
;

	length
		MD5_BYTE1 3
		PAT_KEY 8
		ORDER_KEY 8
		SPECIMEN_SOURCE_SNOMED_CODE $ 6
		SPECIMEN_SOURCE_SNOMED_DESC $ 58
		COLLECTION_DAY_NUMBER 8
		COLLECTION_TIME_OF_DAY 8
		LAB_TEST_LOINC_CODE $ 10
		LAB_TEST_LOINC_DESC $ 128 /* $ 134 */
		LAB_TEST_RESULT $ 96 /*$ 220*/
		RESULT_UNIT $ 17
		FLAGS $ 2
		RESULT_DAY_NUMBER 8
		RESULT_TIME_OF_DAY 8
		REFERENCE_INTERVAL $ 16 /*$ 126*/
		NUMERIC_VALUE 8
		NUMERIC_VALUE_OPERATOR $ 2
	;

	format
		MD5_BYTE1 BEST3.
		PAT_KEY BEST12.
		ORDER_KEY BEST12.
		SPECIMEN_SOURCE_SNOMED_CODE $6.
		SPECIMEN_SOURCE_SNOMED_DESC $58.
		COLLECTION_DAY_NUMBER BEST12.
		COLLECTION_TIME_OF_DAY TIME20.3
		LAB_TEST_LOINC_CODE $10.
		LAB_TEST_LOINC_DESC $128. /*$134.*/
		LAB_TEST_RESULT $96. /*$220.*/
		RESULT_UNIT $17.
		FLAGS $2.
		RESULT_DAY_NUMBER BEST12.
		RESULT_TIME_OF_DAY TIME20.3
		REFERENCE_INTERVAL $16. /*$126.*/
		NUMERIC_VALUE BEST12.
		NUMERIC_VALUE_OPERATOR $2.
	;

	set
		%do i=1 %to &memcount.;
		%let thismem = %sysfunc(scan(%superq(mnames),%unquote(&i.)));
			ORIG.%unquote(&thismem.)(obs=max)
		%end;
		END=DONE
	;

	MD5_BYTE1 = rank (char (md5 (strip (PAT_KEY)), 1));

			%do j=0 %to 255;
				%if &j. GT 0 %then %do;
				else
				%end;
				if (MD5_BYTE1 EQ &j.) then output LAB.LAB_%unquote(&j.);
			%end;

	drop
		_ctod
		_rtod
	;
run;
%if &syserr. GT 4 %then %abort;

%do j=0 %to 255;
PROC SQL;
	CREATE TABLE LAB.LAB_%unquote(&j.) AS
	SELECT
		DISTINCT *
	FROM
		LAB.LAB_%unquote(&j.)
	ORDER BY
		MD5_BYTE1
		,PAT_KEY
		,COLLECTION_DAY_NUMBER
		,COLLECTION_TIME_OF_DAY
		,RESULT_DAY_NUMBER
		,RESULT_TIME_OF_DAY
		,ORDER_KEY
	;
QUIT;
%if &SQLRC GT 4 %then %abort;
%end;
%mend mac_diag_v2;
%mac_diag_v2;
