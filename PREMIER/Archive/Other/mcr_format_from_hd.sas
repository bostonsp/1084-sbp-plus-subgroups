﻿*********************************************************************************************************;
*   Program: mcr_format_from_hd.sas                                                                     *;
*   Verson :  0.3                                                                                       *;
*                                                                                                       *;
*   Description:  macro to convert Cerner ID fields directly to descriptions.                           *;
*                                                                                                       *;
*   Requirements: Numeric ID field in a dictionary file.                                                *;
*                                                                                                       *;
*   Inputs:                                                                                             *;
*     fmt_name  Name of the new format.                                                                 *;
*     dict_tbl_in Name of dictionary table to be used.                                                  *;
*     id_field  Name of id field from dictionary table.                                                 *;
*     desc_field Name of description field from dictionary table.                                       *;
*                                                                                                       *;
*   Outputs:                                                                                            *;
*     Format converting id field to description.                                                        *;
*                                                                                                       *;
*   Author : jhayes                                                                                     *;
*                                                                                                       *;
*   Created: 08/28/2014                                                                                 *;
*                                                                                                       *;
*   Revision history:                                                                                   *;
*   Date        Ver         Programmer  Description                                                     *;
*	160312		0.4			DKH			Added fmt_lib option with default to work *;
*	02/03/2016	0.3			DKH			HLO was formerly zero instead of capital O, now fixed			*;
*	08/28/2014	0.2			DKH			mend and dict_tbl typo fix, added sample calls					*;
*   08/28/2014  0.1         jhayes      initial implementation                                          *;
*                                                                                                       *;
*********************************************************************************************************;

%macro mcr_format_from_hd(
	fmt_name,
	dict_tbl,
 	id_field,
 	desc_field,
	fmt_lib=WORK)
;

%*******************************************************************************************************%;
%**  Sort dictionary table by id field.  Keep only id and description fields.                         **%;
%*******************************************************************************************************%;

proc sort
	data = &dict_tbl.
	out = srtd_tbl
		(keep =
			&id_field.
			&desc_field.)
;

	by 
		&id_field
	;

run;	 
	
%*******************************************************************************************************%;
%**  Create cntlin dataset for use in proc format.                                                    **%;
%*******************************************************************************************************%;	

data cntlin
	(keep =
		fmtname
		type
		hlo
		start
		label)
;
	retain 
		fmtname "&fmt_name."
		type 'N'
	;

	set srtd_tbl
		end = lastrec
	;

	start = &id_field.;
	label = &desc_field.;
	%*label = substr(&desc_field.,1,32);

	output;

	if lastrec then do;
		hlo = 'O';
		start = 0;
		label = 'No Description';
		output;
	end;
run;

%*******************************************************************************************************%;
%**  Create format using proc format.                                                                 **%;
%*******************************************************************************************************%;

proc format cntlin = cntlin library=&fmt_lib.;
run;

%mend mcr_format_from_hd;

%*mcr_format_from_hd(
	fmt_name = DXTYPFMT,
	dict_tbl=HFD2016A.HF_D_DIAGNOSIS,
 	id_field=DIAGNOSIS_ID,
 	desc_field=DIAGNOSIS_TYPE)
;



%*mcr_format_from_hd(
	fmt_name=DCGDFMT,
	dict_tbl=HFD2014B.HF_D_DISCHG_DISP,
 	id_field=DISCHG_DISP_ID,
 	desc_field=DISCHG_DISP_CODE_DESC)
;

%*mcr_format_from_hd(
	fmt_name=PTYPFMT,
	dict_tbl=HFD2014B.HF_D_PATIENT_TYPE,
 	id_field=PATIENT_TYPE_ID,
 	desc_field=PATIENT_TYPE_DESC)
;

%*mcr_format_from_hd(
   fmt_name=CD10DXDES,
   dict_tbl=ICD10_CM_DIM_TABLE,
   id_field=DIAGNOSIS_ID,
   desc_field=CM_CODE_DESCRIPTION)
;

%*mcr_format_from_hd(
   fmt_name=CD10DXL1C,
   dict_tbl=ICD10_CM_DIM_TABLE,
   id_field=DIAGNOSIS_ID,
   desc_field=MULTI_CCS_LVL_1)
;

%*mcr_format_from_hd(
   fmt_name=CD10DXL1L,
   dict_tbl=ICD10_CM_DIM_TABLE,
   id_field=DIAGNOSIS_ID,
   desc_field=MULTI_CCS_LVL_1_LABEL)
;

%*mcr_format_from_hd(
   fmt_name=CD10DXL2C,
   dict_tbl=ICD10_CM_DIM_TABLE,
   id_field=DIAGNOSIS_ID,
   desc_field=MULTI_CCS_LVL_2)
;

%*mcr_format_from_hd(
   fmt_name=CD10DXL2L,
   dict_tbl=ICD10_CM_DIM_TABLE,
   id_field=DIAGNOSIS_ID,
   desc_field=MULTI_CCS_LVL_2_LABEL)
;

%*mcr_format_from_hd(
   fmt_name=CD10PRDES,
   dict_tbl=ICD10_PCS_DIM_TABLE,
   id_field=PROCEDURE_ID,
   desc_field=PCS_CODE_DESCRIPTION)
;

%*mcr_format_from_hd(
   fmt_name=CD10PRL1C,
   dict_tbl=ICD10_PCS_DIM_TABLE,
   id_field=PROCEDURE_ID,
   desc_field=MULTI_CCS_LVL_1)
;

%*mcr_format_from_hd(
   fmt_name=CD10PRL1L,
   dict_tbl=ICD10_PCS_DIM_TABLE,
   id_field=PROCEDURE_ID,
   desc_field=MULTI_CCS_LVL_1_LABEL)
;

%*mcr_format_from_hd(
   fmt_name=CD10PRL2C,
   dict_tbl=ICD10_PCS_DIM_TABLE,
   id_field=PROCEDURE_ID,
   desc_field=MULTI_CCS_LVL_2)
;

%*mcr_format_from_hd(
   fmt_name=CD10PRL2L,
   dict_tbl=ICD10_PCS_DIM_TABLE,
   id_field=PROCEDURE_ID,
   desc_field=MULTI_CCS_LVL_2_LABEL)
;
