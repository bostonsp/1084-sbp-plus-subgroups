﻿/*---------------------------------------------------------------------------------
Project:            0867 Grifols Sepsis and Cirrhosis in Premier

Program:            ____

Function:           Start by pulling all of the encounters/patients that are
					related to the sepsis categories established in 0661/0827

Dependencies:		____

Dependents:			____

Author:             ____

Date Completed:     ____

SAS Version:        ____   

Platform:           ____

Macros Called:      ____ 

Macro Parameters:   ____

Comments:           ____

Example:			____

Revisions:
date	name	what
191119	XM		Initial Version
191124	XM		Updated after being reformatted
191204	XM		Update to PK code to move SEPSIS category to within data step
200116	XM		Update the data to the new data set  making the code run for all
				segments
---------------------------------------------------------------------------------*/

/*Look at the sepsis code that was used in the 0661
project. Inlcude the same ICD codes in pulling the
sepsis patients.*/
proc format;
	value $SEPDX

	/*"995.92",
	"785.52",
	"998.02",*/
	"R65.20",
	"R65.21",
	"T81.12XA" = 'SEPSIS'

	other = 'OTHER'
	;
run; 

/*Based on the setting up the data with md5_byte1
we can pull patients/visits of interest using a loop
for 255 sections*/
%macro sep_dx;
%local i;

	/*The sepsis codes that we have are all ICD codes
	and for DX so use the diagnosis DX section of the
	database file.*/
	data sepsis1;

		set
			%do i=0 %to 255;
				DIAG.DIAG_&i.(
				)
			%end;
		;
		
		SEPSIS_CATEGORY = put(ICD_CODE, $SEPDX.);
		IF SEPSIS_CATEGORY ne 'OTHER' then output;

	run;

%mend sep_dx;
%sep_dx; 
* R = 879924 (<6 min)*;

proc sql;
	create table sepsis2 as
	select  distinct medrec_key,
			pat_key,
			ICD_PRI_SEC
	from sepsis1
	order medrec_key, pat_key;
quit;
* R = 876440 *;

proc freq data=sepsis2;
	table ICD_PRI_SEC / missing norow nocol;
run;
/*
A 11533 1.32 11533 1.32 
P 7426 0.85 18959 2.16 
S 857481 97.84 876440 100.00 
*/

data sepsis3;
	set sepsis2;
	ICD_A = 0;
	ICD_P = 0;
	ICD_S = 0;

	if ICD_PRI_SEC = "A" then ICD_A = 1;
	if ICD_PRI_SEC = "P" then ICD_P = 1;
	if ICD_PRI_SEC = "S" then ICD_S = 1;
run;
* R = 876440 *;

proc sql;
	create table sepsis4 as
	select  medrec_key,
			pat_key,
			max(ICD_A) as ICD_A format=1.,
			max(ICD_P) as ICD_P format=1.,
			max(ICD_S) as ICD_S format=1.
	from sepsis3
	group medrec_key, pat_key
	order medrec_key, pat_key;
quit;
* R = 866264 *;

proc sql;
	select  distinct ICD_A,
			ICD_P,
			ICD_S,
			count(*) as count
	from sepsis4
	group by 1, 2, 3;
quit;
/*
ICD_A ICD_P ICD_S count 
0 0 1 848724 
0 1 0 5981 
0 1 1 26 
1 0 0 1385 
1 0 1 8729 
1 1 0 1417 
1 1 1 2
*/

data SEP.Sepsis;
	set sepsis4;
run;
* R = 866264 *;

proc sql;
	select 	count(distinct medrec_key) as P format=10.,
			count(distinct pat_key) as V format=10.
	from SEP.Sepsis;
quit;
* P = 778721, V = 866264 *;

**************************************************  End  **************************************************;

%macro sep_dx;
%local i;

	/*The sepsis codes that we have are all ICD codes
	and for DX so use the diagnosis DX section of the
	database file.*/
	data temp1;

		set
			%do i=0 %to 31;
				DIAG.DIAG_&i.(
				)
			%end;
		;
		
	run;

%mend sep_dx;
%sep_dx; 

proc freq data=temp1;
	table ICD_PRI_SEC ICD_POA / missing norow nocol;
run;
/*
1 = Exempt from POA reporting
9 = No code submitted
N = No (not present on admission)
U = Unknown (documentation insufficient)
W = Clinically undetermined
Y = Yes (present on admission)
*/
