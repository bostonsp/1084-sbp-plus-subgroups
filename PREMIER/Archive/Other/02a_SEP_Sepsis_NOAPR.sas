﻿/*---------------------------------------------------------------------------------
Project:            0867 Grifols Sepsis and Cirrhosis in Premier

Program:            ____

Function:           Start by pulling all of the noapr information for the 
					high cut of cirrhosis patients and encounters

Dependencies:		____

Dependents:			____

Author:             ____

Date Completed:     ____

SAS Version:        ____   

Platform:           ____

Macros Called:      ____ 

Macro Parameters:   ____

Comments:           ____

Example:			____

Revisions:
date	name	what
191124	XM		Initial Version
200116	XM		Updated for the new set of data 
---------------------------------------------------------------------------------*/

* ==================================  Step 1. NOAPR  ================================== *;
/*With the large cohort of patients before the selection, pull the DX tables*/
%macro loop_noapr (dsin = , start_seg = , end_seg = );
%local i;
	
	/*Start by selecting visit data for patients of interest*/
	%do i = &start_seg. %to &end_seg.;
		proc sql;
			create view noapr_&i. as
			select distinct t2.*
			from &dsin. as t1 inner join NOAPR.NOAPR_&i. as t2 on
				t1.medrec_key = t2.medrec_key and t1.pat_key = t2.pat_key
			order by t1.medrec_key, t1.pat_key;
		quit; 
	%end;
	
	/*Add the datasets together*/
	data noapr1 / view = noapr1;
		set noapr_&start_seg. - noapr_&end_seg.;
	run; 

	proc sort data = noapr1 out = noapr2 presorted noduprec;
		by medrec_key pat_key;
	run;

	%do i = &start_seg. %to &end_seg.;
		/*Delete any datasets have same name as views later*/
		proc datasets lib = work nolist;
			delete noapr_&i. (memtype = view) noapr1 (memtype = view);
		quit;
		run;
	%end;

%mend loop_noapr; 

%loop_noapr(dsin = SEP.Sepsis, start_seg = 0, end_seg = 255);
* R = 866266 (<6 min) *;

proc sql;
	select 	count(distinct MEDREC_KEY) as P format=10.,
			count(distinct PAT_KEY) as V format=10.
	from noapr2;
quit;
* P = 778721, V = 866264 *;



* ==================================  Step 2. Expired  ================================== *;
/* Data cleaning: Expired status over time */
%macro loop_NOAPR;
%local i;
	data SEP.expired1;
		set
			%do i=0 %to 255;
				NOAPR.NOAPR_&i.
			%end;
		;
	run;
%mend loop_NOAPR;
%loop_NOAPR;
* R = 289907562 (<3 min) *;

data SEP.expired2;
	set SEP.expired1;
	if lowcase(put(DISC_STATUS, DISS.)) =: "expired" then Expired = 1;
	else Expired = 0; 
run;
* R = 289907562 (<6 min) *;
* It doesn't make sense to have data after patients were expired/deceased *;
* However, it makes sense to have data after discharged/transferred to other facilities (e.g., Hospice, SNF) *;

proc sql;
	create table SEP.expired3 as
	select 	distinct MEDREC_KEY,
			DISC_MON
	from SEP.expired2
	where Expired = 1
	order by MEDREC_KEY, DISC_MON;
quit;
* R = 727063 *;

proc sql;
	select 	count(distinct MEDREC_KEY) as P format=10.
	from SEP.expired3;
quit;
* P = 726375 *;

proc sql;
	create table zzz1 as
	select  *,
			count(*) as count
	from SEP.expired3
	group by MEDREC_KEY
	having count(*) > 1
	order by MEDREC_KEY;
quit;
* R = 1142 *;

data SEP.expired4;
	set SEP.expired3;
	by MEDREC_KEY;
	if first.MEDREC_KEY;
run;
* R = 726375 *;



* ==================================  Step 3. Combine  ================================== *;
proc sql;
	create table noapr3 as
	select 	t1.*,
			t2.DISC_MON as DISC_MON_EXPIRED
	from noapr2 as t1 left join SEP.expired4 as t2 on t1.MEDREC_KEY = t2.MEDREC_KEY
	order by t1.MEDREC_KEY, t1.ADM_MON, t1.DISC_MON, t1.DISC_MON_SEQ;
quit;
* R = 866266 *;

data noapr4;
	set noapr3;
	*if . < DISC_MON_EXPIRED < ADM_MON;
	if . < DISC_MON_EXPIRED < ADM_MON then delete;
run;
* R = 866224 *;

proc sql;
	select 	count(distinct MEDREC_KEY) as P format=10.,
			count(distinct PAT_KEY) as V format=10.
	from noapr4;
quit;
* P = 778688, V = 866222 *;

proc sql;
	create table zzz2 as
	select  *,
			count(*) as count
	from noapr4
	group by MEDREC_KEY, PAT_KEY
	having count(*) > 1
	order by MEDREC_KEY, PAT_KEY;
quit;
* R = 4 *;

data noapr5;
	set noapr4;
	if MEDREC_KEY = "3233903" and PAT_KEY = 809915010 and DISC_MON = 2017206 then delete;
	if MEDREC_KEY = "7004219" and PAT_KEY = 821680418 and DISC_MON = 2017206 then delete;
run; 
* R = V = 866222 *;
 
/*Save this data*/
proc sort data=noapr5 out=SEP.Sepsis_NOAPR presorted;
	by medrec_key pat_key disc_mon_seq;
run; 
* R = V = 866222 *;

**************************************************  End  **************************************************;



/*
also check 1 visit (encounter) has 2 different start/end date
*/


