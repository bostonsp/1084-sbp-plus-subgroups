﻿/*---------------------------------------------------------------------------------
Project:            0867 Grifols Sepsis and Cirrhosis in Premier

Program:            ____ 

Function:           Start by pulling all of the lab information for the 
					high cut of cirrhosis patients and encounters

Dependencies:		____

Dependents:			____

Author:             ____

Date Completed:     ____

SAS Version:        ____   

Platform:           ____

Macros Called:      ____ 

Macro Parameters:   ____

Comments:           ____

Example:			____

Revisions:
date	name	what
200701	SH		Initial Version
---------------------------------------------------------------------------------*/

/*With the large cohort of patients before the selection, pull the LAB tables*/
%macro loop_lab (dsin = , start_seg = , end_seg = );
%local i;
	
	/*Start by selecting visit data for patients of interest*/
	%do i = &start_seg. %to &end_seg.;
		proc sql;
			create view lab_&i. as
			select distinct t2.*
			from &dsin. as t1 inner join LAB.LAB_&i. as t2 on
				t1.pat_key = t2.pat_key
			order by t1.pat_key;
		quit; 
	%end;
	
	/*Add the datasets together*/
	data lab1 / view = lab1;
		set lab_&start_seg. - lab_&end_seg.;
	run; 

	proc sort data = lab1 out = lab2 presorted noduprec;
		by pat_key;
	run;

	%do i = &start_seg. %to &end_seg.;
		/*Delete any datasets have same name as views later*/
		proc datasets lib = work nolist;
			delete lab_&i. (memtype = view) lab1 (memtype = view);
		quit;
		run;
	%end;

%mend loop_lab; 

%loop_lab(dsin = SEP.Sepsis_NOAPR, start_seg = 0, end_seg = 255);
* R = 43060766 (<39 min) *;

proc sql;
	create table lab3 as
	select 	distinct t2.medrec_key,
			t1.*
	from lab2 as t1 left join SEP.Sepsis_NOAPR as t2 on t1.pat_key = t2.pat_key
	order by t2.medrec_key, t1.pat_key;
quit;
* R = 43060766 (<9 min) *;

/*Save this data*/
proc sort data=lab3 out=SEP.Sepsis_LAB presorted;
	by medrec_key pat_key;
run; 

proc sql;
	select 	count(distinct medrec_key) as P format=10.,
			count(distinct pat_key) as V format=10.
	from SEP.Sepsis_LAB;
quit;
* P = 83990, V = 91075 *;

**************************************************  End  **************************************************;
