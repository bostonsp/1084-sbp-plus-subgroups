﻿/*-----------------------------------------------------------------------------
Project:            ____

Program:            0867_04_Import_PATBILL.sas

Function:           ____

Dependencies:		GR_YYYYM_PATBILL.SAS7BDAT
					key table NOAPR.MEDREC_KEYTABLE

Dependents:			____

Outputs:			____

Author:             ____

SAS Version:        ____   

Comments:           ____

Revisions:
date	name	what
191120	XM		First version
200109	XM		Updated to import the new data set
-----------------------------------------------------------------------------*/

libname ORIG V9 "G:\PAS23" access=readonly;
libname NOAPR V9 "S:\0867_PREMIER\201911\NOAPR\" access = readonly;
libname BILL V9 "S:\0867_PREMIER\201911\BILL\";

proc options group = memory; 
run;
/*	select memname metadata into global macro variable for macro later on	*/
PROC SQL NOPRINT;
	SELECT
		DISTINCT MEMNAME into :mnames SEPARATED BY ' '
	FROM
		SASHELP.VCOLUMN
	WHERE
		STRIP(LIBNAME) EQ 'ORIG'
		AND UPCASE(STRIP(MEMNAME)) CONTAINS 'PATBILL'
	;
QUIT;
%put &mnames.;

proc contents data = ORIG.PHD_20161_PATBILL varnum;
run; 

/*The data set becomes too large to be processed. Try
cutting the input data based on md5_byte1*/
data medrec_keytable_0_63;
	set NOAPR.MEDREC_KEYTABLE;
	if md5_byte1 GE 0 and md5_byte1 LE 63 then output;
run; 

%macro mac_bill;
%global &mnames;
%local i memcount thismem j;

%let memcount=%sysfunc(countw(%superq(mnames)));
%put NOTE: &memcount.;
data
	%do j=0 %to 63;
		BILL.BILL_%unquote(&j.)
	%end;
;

	if (1 EQ 0) then do;
		set work.medrec_keytable_0_63;
		set ORIG.PHD_20161_PATBILL;
	end;

	if (_N_ EQ 1) then do;

		declare hash hk(dataset:"work.medrec_keytable_0_63");
		rc=hk.defineKey('PAT_KEY');
		rc=hk.defineData('PAT_KEY','MEDREC_KEY','MD5_BYTE1');
		rc=hk.defineDone();

		/*	declare fact table	*/
		declare hash hf(ordered:'A',hashexp:10);
		declare hiter hfi('hf');
		rc1=hf.defineKey(
			'MD5_BYTE1'
			,'MEDREC_KEY'
			,'PAT_KEY'
			,'STD_CHG_CODE'
			,'HOSP_CHG_ID'
			,'SERV_DAY'
			,'HOSP_QTY'
			,'STD_QTY'
			,'BILL_CHARGES'
			,'BILL_COST'
			,'BILL_VAR_COST'
			,'BILL_FIX_COST'
		);
		rc1=hf.defineData(
			'MD5_BYTE1'
			,'MEDREC_KEY'
			,'PAT_KEY'
			,'STD_CHG_CODE'
			,'HOSP_CHG_ID'
			,'SERV_DAY'
			,'HOSP_QTY'
			,'STD_QTY'
			,'BILL_CHARGES'
			,'BILL_COST'
			,'BILL_VAR_COST'
			,'BILL_FIX_COST'
		);
		rc1=hf.defineDone();

	end;

	set
		%do i=1 %to &memcount.;
		%let thismem = %sysfunc(scan(%superq(mnames),%unquote(&i.)));
			ORIG.%unquote(&thismem.)/*(obs=10000)*/
		%end;
		END=DONE
	;

	rc=hk.find(key:PAT_KEY);

	if (rc EQ 0) then do;
		/*MD5_BYTE1 = rank (char (md5 (strip (MEDREC_KEY)), 1));*/
		rc=hf.replace();
	end;

	if (DONE) then do;
		/*rc1=hf.output(dataset:"work.DIAG(compress=no)");*/
		rc1=hfi.first();
		do while (rc1 EQ 0);
			%do j=0 %to 63;
				%if &j. GT 0 %then %do;
				else
				%end;
				if (MD5_BYTE1 EQ &j.) then output BILL.BILL_%unquote(&j.);
			%end;
			rc1=hfi.next();
		end;
	end;

	drop
		rc
		rc1
	;
run;
%if &syserr. GT 4 %then %abort;
%mend mac_bill;
%mac_bill;

data medrec_keytable_64_127;
	set NOAPR.MEDREC_KEYTABLE;
	if md5_byte1 GE 64 and md5_byte1 LE 127 then output;
run; 

%macro mac_bill_64_127;
%global &mnames;
%local i memcount thismem j;

%let memcount=%sysfunc(countw(%superq(mnames)));
%put NOTE: &memcount.;
data
	%do j=64 %to 127;
		BILL.BILL_%unquote(&j.)
	%end;
;

	if (1 EQ 0) then do;
		set work.medrec_keytable_64_127;
		set ORIG.PHD_20161_PATBILL;
	end;

	if (_N_ EQ 1) then do;

		declare hash hk(dataset:"work.medrec_keytable_64_127");
		rc=hk.defineKey('PAT_KEY');
		rc=hk.defineData('PAT_KEY','MEDREC_KEY','MD5_BYTE1');
		rc=hk.defineDone();

		/*	declare fact table	*/
		declare hash hf(ordered:'A',hashexp:10);
		declare hiter hfi('hf');
		rc1=hf.defineKey(
			'MD5_BYTE1'
			,'MEDREC_KEY'
			,'PAT_KEY'
			,'STD_CHG_CODE'
			,'HOSP_CHG_ID'
			,'SERV_DAY'
			,'HOSP_QTY'
			,'STD_QTY'
			,'BILL_CHARGES'
			,'BILL_COST'
			,'BILL_VAR_COST'
			,'BILL_FIX_COST'
		);
		rc1=hf.defineData(
			'MD5_BYTE1'
			,'MEDREC_KEY'
			,'PAT_KEY'
			,'STD_CHG_CODE'
			,'HOSP_CHG_ID'
			,'SERV_DAY'
			,'HOSP_QTY'
			,'STD_QTY'
			,'BILL_CHARGES'
			,'BILL_COST'
			,'BILL_VAR_COST'
			,'BILL_FIX_COST'
		);
		rc1=hf.defineDone();

	end;

	set
		%do i=1 %to &memcount.;
		%let thismem = %sysfunc(scan(%superq(mnames),%unquote(&i.)));
			ORIG.%unquote(&thismem.)/*(obs=10000)*/
		%end;
		END=DONE
	;

	rc=hk.find(key:PAT_KEY);

	if (rc EQ 0) then do;
		/*MD5_BYTE1 = rank (char (md5 (strip (MEDREC_KEY)), 1));*/
		rc=hf.replace();
	end;

	if (DONE) then do;
		/*rc1=hf.output(dataset:"work.DIAG(compress=no)");*/
		rc1=hfi.first();
		do while (rc1 EQ 0);
			%do j = 64 %to 127;
				%if &j. GT 64 %then %do;
				else
				%end;
				if (MD5_BYTE1 EQ &j.) then output BILL.BILL_%unquote(&j.);
			%end;
			rc1=hfi.next();
		end;
	end;

	drop
		rc
		rc1
	;
run;
%if &syserr. GT 4 %then %abort;
%mend mac_bill_64_127;
%mac_bill_64_127;

data medrec_keytable_128_191;
	set NOAPR.MEDREC_KEYTABLE;
	if md5_byte1 GE 128 and md5_byte1 LE 191 then output;
run; 

%macro mac_bill_128_191;
%global &mnames;
%local i memcount thismem j;

%let memcount=%sysfunc(countw(%superq(mnames)));
%put NOTE: &memcount.;
data
	%do j=128 %to 191;
		BILL.BILL_%unquote(&j.)
	%end;
;

	if (1 EQ 0) then do;
		set work.medrec_keytable_128_191;
		set ORIG.PHD_20161_PATBILL;
	end;

	if (_N_ EQ 1) then do;

		declare hash hk(dataset:"work.medrec_keytable_128_191");
		rc=hk.defineKey('PAT_KEY');
		rc=hk.defineData('PAT_KEY','MEDREC_KEY','MD5_BYTE1');
		rc=hk.defineDone();

		/*	declare fact table	*/
		declare hash hf(ordered:'A',hashexp:10);
		declare hiter hfi('hf');
		rc1=hf.defineKey(
			'MD5_BYTE1'
			,'MEDREC_KEY'
			,'PAT_KEY'
			,'STD_CHG_CODE'
			,'HOSP_CHG_ID'
			,'SERV_DAY'
			,'HOSP_QTY'
			,'STD_QTY'
			,'BILL_CHARGES'
			,'BILL_COST'
			,'BILL_VAR_COST'
			,'BILL_FIX_COST'
		);
		rc1=hf.defineData(
			'MD5_BYTE1'
			,'MEDREC_KEY'
			,'PAT_KEY'
			,'STD_CHG_CODE'
			,'HOSP_CHG_ID'
			,'SERV_DAY'
			,'HOSP_QTY'
			,'STD_QTY'
			,'BILL_CHARGES'
			,'BILL_COST'
			,'BILL_VAR_COST'
			,'BILL_FIX_COST'
		);
		rc1=hf.defineDone();

	end;

	set
		%do i=1 %to &memcount.;
		%let thismem = %sysfunc(scan(%superq(mnames),%unquote(&i.)));
			ORIG.%unquote(&thismem.)/*(obs=10000)*/
		%end;
		END=DONE
	;

	rc=hk.find(key:PAT_KEY);

	if (rc EQ 0) then do;
		/*MD5_BYTE1 = rank (char (md5 (strip (MEDREC_KEY)), 1));*/
		rc=hf.replace();
	end;

	if (DONE) then do;
		/*rc1=hf.output(dataset:"work.DIAG(compress=no)");*/
		rc1=hfi.first();
		do while (rc1 EQ 0);
			%do j = 128 %to 191;
				%if &j. GT 128 %then %do;
				else
				%end;
				if (MD5_BYTE1 EQ &j.) then output BILL.BILL_%unquote(&j.);
			%end;
			rc1=hfi.next();
		end;
	end;

	drop
		rc
		rc1
	;
run;
%if &syserr. GT 4 %then %abort;
%mend mac_bill_128_191;
%mac_bill_128_191;

data medrec_keytable_192_255;
	set NOAPR.MEDREC_KEYTABLE;
	if md5_byte1 GE 192 and md5_byte1 LE 255 then output;
run; 

%macro mac_bill_192_255;
%global &mnames;
%local i memcount thismem j;

%let memcount=%sysfunc(countw(%superq(mnames)));
%put NOTE: &memcount.;
data
	%do j=192 %to 255;
		BILL.BILL_%unquote(&j.)
	%end;
;

	if (1 EQ 0) then do;
		set work.medrec_keytable_192_255;
		set ORIG.PHD_20161_PATBILL;
	end;

	if (_N_ EQ 1) then do;

		declare hash hk(dataset:"work.medrec_keytable_192_255");
		rc=hk.defineKey('PAT_KEY');
		rc=hk.defineData('PAT_KEY','MEDREC_KEY','MD5_BYTE1');
		rc=hk.defineDone();

		/*	declare fact table	*/
		declare hash hf(ordered:'A',hashexp:10);
		declare hiter hfi('hf');
		rc1=hf.defineKey(
			'MD5_BYTE1'
			,'MEDREC_KEY'
			,'PAT_KEY'
			,'STD_CHG_CODE'
			,'HOSP_CHG_ID'
			,'SERV_DAY'
			,'HOSP_QTY'
			,'STD_QTY'
			,'BILL_CHARGES'
			,'BILL_COST'
			,'BILL_VAR_COST'
			,'BILL_FIX_COST'
		);
		rc1=hf.defineData(
			'MD5_BYTE1'
			,'MEDREC_KEY'
			,'PAT_KEY'
			,'STD_CHG_CODE'
			,'HOSP_CHG_ID'
			,'SERV_DAY'
			,'HOSP_QTY'
			,'STD_QTY'
			,'BILL_CHARGES'
			,'BILL_COST'
			,'BILL_VAR_COST'
			,'BILL_FIX_COST'
		);
		rc1=hf.defineDone();

	end;

	set
		%do i=1 %to &memcount.;
		%let thismem = %sysfunc(scan(%superq(mnames),%unquote(&i.)));
			ORIG.%unquote(&thismem.)/*(obs=10000)*/
		%end;
		END=DONE
	;

	rc=hk.find(key:PAT_KEY);

	if (rc EQ 0) then do;
		/*MD5_BYTE1 = rank (char (md5 (strip (MEDREC_KEY)), 1));*/
		rc=hf.replace();
	end;

	if (DONE) then do;
		/*rc1=hf.output(dataset:"work.DIAG(compress=no)");*/
		rc1=hfi.first();
		do while (rc1 EQ 0);
			%do j = 192 %to 255;
				%if &j. GT 192 %then %do;
				else
				%end;
				if (MD5_BYTE1 EQ &j.) then output BILL.BILL_%unquote(&j.);
			%end;
			rc1=hfi.next();
		end;
	end;

	drop
		rc
		rc1
	;
run;
%if &syserr. GT 4 %then %abort;
%mend mac_bill_192_255;
%mac_bill_192_255;

%macro setmeta;
%local j;
%do j=0 %to 255;
	proc sort data=BILL.BILL_%unquote(&j.) nodupkey presorted;
	by _ALL_
	;
	run;
	%if &syserr. GT 4 %then %abort;
%end;
%mend setmeta;
%setmeta;
