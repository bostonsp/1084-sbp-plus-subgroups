﻿/*---------------------------------------------------------------------------------
Project:            0867 Grifols Sepsis and Cirrhosis in Premier

Program:            00_Startup_Premier.sas 

Function:           ____

Dependencies:		____

Dependents:			____

Author:             ____

Date Completed:     ____

SAS Version:        ____   

Platform:           ____

Macros Called:      ____ 

Macro Parameters:   ____

Comments:           ____

Example:			____

Revisions:
date	name	what
191119	XM		Initial Version
191121	XM		Updated after data has been segmented
191209	XM		Updated to point to the new data set
---------------------------------------------------------------------------------*/

/*Create a libname for the premier data*/
/*LIBNAME PRE "S:\0867_PREMIER\Premier data Jan 2019" ACCESS = READONLY;*/

/*Data for the patient key and other demographic information*/
libname NOAPR V9 "S:\0867_PREMIER\201911\NOAPR\" ACCESS = READONLY; 

/*Diagnosis data*/
libname DIAG V9 "S:\0867_PREMIER\201911\DIAG\" ACCESS = READONLY;

/*Procedure data*/
libname PROC V9 "S:\0867_PREMIER\201911\PROC\" ACCESS = READONLY;

/*CPT data */
libname CPT V9 "S:\0867_PREMIER\201911\CPT\" ACCESS = READONLY;

/*Definition tables*/
libname DEF V9 "S:\0867_PREMIER\201911\DEF\" ACCESS = READONLY;

/*Billing tables*/
libname BILL V9 "S:\0867_PREMIER\201911\BILL\" ACCESS = READONLY;

/*Create a libname for cirrhosis and sepsis to keep data separate and to
allow for tracking*/
LIBNAME CIR1 "F:\0867\Cirrhosis\Cohort1";
LIBNAME CIR2 "F:\0867\Cirrhosis\Cohort2";

LIBNAME SEP1 "F:\0867\Sepsis\Cohort1";
LIBNAME SEP2 "F:\0867\Sepsis\Cohort2";

libname FMT0867 V9 "F:\0867\FMT0867" ACCESS=READONLY;
options fmtsearch=(FMT0867 WORK LIBRARY);
