﻿/*-----------------------------------------------------------------------------
Project:            ____

Program:            0867_01_MEDREC_KEY_table.sas 

Function:           ____

Dependencies:		____

Dependents:			____

Outputs:			____

Author:             ____

SAS Version:        ____   

Comments:           ____

Revisions:
date	name	what
191204	DKH		Replace hardcode in "set rest of pdv w metadata from dsin"
				with using the first name in &mnames.
191204	DKH		Update libnames for PAS23/201911
191120	DKH		First version
-----------------------------------------------------------------------------*/
libname ORIG V9 "G:\PAS23" access=readonly;
libname NOAPR V9 "S:\0867_PREMIER\201911\NOAPR\";

/*	select memname metadata into global macro variable for macro later on	*/
PROC SQL NOPRINT;
	SELECT
		DISTINCT MEMNAME into :mnames SEPARATED BY ' '
	FROM
		SASHELP.VCOLUMN
	WHERE
		STRIP(LIBNAME) EQ 'ORIG'
		AND UPCASE(STRIP(MEMNAME)) CONTAINS 'NOAPR'
	;
QUIT;
%put &mnames.;

%macro mac_pk;
%global &mnames;
%local i memcount thismem j;

%let memcount=%sysfunc(countw(%superq(mnames)));
%put NOTE: &memcount.;
%*abort;

/*	create medrec_keytable	*/
data
	%do j=0 %to 255;
		NOAPR.NOAPR_%unquote(&j.)(compress=no)
	%end;
;

	/* declare md5_byte1 as varnum 1	*/
	length
		MD5_BYTE1	3
		/*MEDREC_KEY $ 14
		PAT_KEY 7*/
	;

	/*	set rest of pdv w metadata from dsin	*/
	if (1 EQ 0) then do;
		set
			%let thismem = %sysfunc(scan(%superq(mnames),%unquote(1)));
			ORIG.%unquote(&thismem.)
		;
	end;

	
	if (_N_ EQ 1) then do;
		/*	declare keytbl	*/
		declare hash hk(ordered:'A',hashexp:10);
		rc=hk.defineKey('MD5_BYTE1','MEDREC_KEY','PAT_KEY');
		rc=hk.defineData('MD5_BYTE1','MEDREC_KEY','PAT_KEY');
		rc=hk.defineDone();

		/*	declare fact table	*/
		declare hash hf(ordered:'A',hashexp:10);
		declare hiter hfi('hf');
		rc1=hf.defineKey(
			'MD5_BYTE1'
			,'MEDREC_KEY'
			,'PAT_KEY'
			
			,'DISC_MON'
			,'DISC_MON_SEQ'
			,'ADM_MON'
			,'PROV_ID'
			,'I_O_IND'
			,'PAT_TYPE'
			,'MS_DRG'
			,'MS_DRG_MDC'
			,'ADM_SOURCE'
			,'POINT_OF_ORIGIN'
			,'ADM_TYPE'
			,'DISC_STATUS'
			,'MART_STATUS'
			,'AGE'
			,'GENDER'
			,'RACE'
			,'HISPANIC_IND'
			,'ADMPHY_SPEC'
			,'ADM_PHY'
			,'ATTPHY_SPEC'
			,'ATT_PHY'
			,'STD_PAYOR'
			,'LOS'
			,'PROJ_WGT'
			,'PAT_CHARGES'
			,'PAT_COST'
			,'PAT_FIX_COST'
			,'PAT_VAR_COST'
		);
		rc1=hf.defineData(
			'MD5_BYTE1'
			,'MEDREC_KEY'
			,'PAT_KEY'
			
			,'DISC_MON'
			,'DISC_MON_SEQ'
			,'ADM_MON'
			,'PROV_ID'
			,'I_O_IND'
			,'PAT_TYPE'
			,'MS_DRG'
			,'MS_DRG_MDC'
			,'ADM_SOURCE'
			,'POINT_OF_ORIGIN'
			,'ADM_TYPE'
			,'DISC_STATUS'
			,'MART_STATUS'
			,'AGE'
			,'GENDER'
			,'RACE'
			,'HISPANIC_IND'
			,'ADMPHY_SPEC'
			,'ADM_PHY'
			,'ATTPHY_SPEC'
			,'ATT_PHY'
			,'STD_PAYOR'
			,'LOS'
			,'PROJ_WGT'
			,'PAT_CHARGES'
			,'PAT_COST'
			,'PAT_FIX_COST'
			,'PAT_VAR_COST'
		);
		rc1=hf.defineDone();

		/*	init values	*/
		call missing(of _ALL_);
	end;

	set
	%do i=1 %to &memcount.;
		%let thismem = %sysfunc(scan(%superq(mnames),%unquote(&i.)));
		ORIG.%unquote(&thismem.)(
			obs=MAX
			/*keep=
				MEDREC_KEY
				PAT_KEY*/
		)
	%end;
		END=DONE
	;

	MD5_BYTE1 = rank (char (md5 (strip (MEDREC_KEY)), 1));

	rc=hk.replace(
		key:MD5_BYTE1
		,key:MEDREC_KEY
		,key:PAT_KEY
		,data:MD5_BYTE1
		,data:MEDREC_KEY
		,data:PAT_KEY
		
	);

	rc1=hf.replace(
		key:MD5_BYTE1
		,key:MEDREC_KEY
		,key:PAT_KEY
		,key:DISC_MON
		,key:DISC_MON_SEQ
		,key:ADM_MON
		,key:PROV_ID
		,key:I_O_IND
		,key:PAT_TYPE
		,key:MS_DRG
		,key:MS_DRG_MDC
		,key:ADM_SOURCE
		,key:POINT_OF_ORIGIN
		,key:ADM_TYPE
		,key:DISC_STATUS
		,key:MART_STATUS
		,key:AGE
		,key:GENDER
		,key:RACE
		,key:HISPANIC_IND
		,key:ADMPHY_SPEC
		,key:ADM_PHY
		,key:ATTPHY_SPEC
		,key:ATT_PHY
		,key:STD_PAYOR
		,key:LOS
		,key:PROJ_WGT
		,key:PAT_CHARGES
		,key:PAT_COST
		,key:PAT_FIX_COST
		,key:PAT_VAR_COST
		,data:MD5_BYTE1
		,data:MEDREC_KEY
		,data:PAT_KEY
		,data:DISC_MON
		,data:DISC_MON_SEQ
		,data:ADM_MON
		,data:PROV_ID
		,data:I_O_IND
		,data:PAT_TYPE
		,data:MS_DRG
		,data:MS_DRG_MDC
		,data:ADM_SOURCE
		,data:POINT_OF_ORIGIN
		,data:ADM_TYPE
		,data:DISC_STATUS
		,data:MART_STATUS
		,data:AGE
		,data:GENDER
		,data:RACE
		,data:HISPANIC_IND
		,data:ADMPHY_SPEC
		,data:ADM_PHY
		,data:ATTPHY_SPEC
		,data:ATT_PHY
		,data:STD_PAYOR
		,data:LOS
		,data:PROJ_WGT
		,data:PAT_CHARGES
		,data:PAT_COST
		,data:PAT_FIX_COST
		,data:PAT_VAR_COST
	);
	
	if (DONE) then do;
		rc=hk.output(dataset:"NOAPR.MEDREC_KEYTABLE");
		/*rc1=hf.output(dataset:"PREM.noapr(compress=char)");*/
		rc1=hfi.first();
		do while (rc1 EQ 0);
			%do j=0 %to 255;
				%if &j. GT 0 %then %do;
				else
				%end;
				if (MD5_BYTE1 EQ &j.) then output NOAPR.NOAPR_%unquote(&j.);
			%end;
			rc1=hfi.next();
		end;
	end;
run;
%if &syserr. GT 4 %then %abort;
%mend mac_pk;
%mac_pk;

%macro setmeta;
%local j;
%do j=0 %to 255;
	proc sort data=NOAPR.NOAPR_%unquote(&j.) (drop = rc rc1) nodupkey presorted;
	by MD5_BYTE1
		MEDREC_KEY
		PAT_KEY
		DISC_MON
		DISC_MON_SEQ
		ADM_MON
		PROV_ID
		I_O_IND
		PAT_TYPE
		MS_DRG
		MS_DRG_MDC
		ADM_SOURCE
		POINT_OF_ORIGIN
		ADM_TYPE
		DISC_STATUS
		MART_STATUS
		AGE
		GENDER
		RACE
		HISPANIC_IND
		ADMPHY_SPEC
		ADM_PHY
		ATTPHY_SPEC
		ATT_PHY
		STD_PAYOR
		LOS
		PROJ_WGT
		PAT_CHARGES
		PAT_COST
		PAT_FIX_COST
		PAT_VAR_COST
	;
	run;
	%if &syserr. GT 4 %then %abort;
%end;
%mend setmeta;
%setmeta;


/*proc sql;
	select count(distinct medrec_key) from ORIG.GR_20111_PAT_NOAPR(
			keep=MEDREC_KEY
		)
	;
quit;*/

proc contents data=ORIG.phd_20163_pat_noapr varnum;
run;

proc univariate data=work.MEDREC_KEYTABLE;
var MD5_BYTE1;
histogram MD5_BYTE1/normal midpoints=0 to 255 by 1;
run;
