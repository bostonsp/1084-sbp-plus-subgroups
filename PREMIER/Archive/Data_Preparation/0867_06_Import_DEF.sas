﻿/*-----------------------------------------------------------------------------
Project:            ____

Program:            0867_06_Import_DEF.sas

Function:           ____

Dependencies:		
					
Dependents:			____

Outputs:			____

Author:             ____

SAS Version:        ____   

Comments:           ____

Revisions:
date	name	what
191206	XM		Updated for the new data received on 191206
191120	XM		Initial version
-----------------------------------------------------------------------------*/

/*The definition tables are not in the same source that we use for SAS.
Running directly without converting would increase the run-time of codes.
Spend time now to read this data in and save it, in order to save time later.*/
libname ORIG V9 "G:\PAS23" access=readonly;
libname NOAPR V9 "S:\0867_PREMIER\201911\NOAPR\" access = readonly;
libname DEF V9 "S:\0867_PREMIER\201911\DEF\";

/*Start with the admission source table*/
proc sort data = ORIG.PHD_LU_ADMSRC out = DEF.DEF_ADMSRC presorted;
	by ADM_SOURCE;
run; 

/*Next is admission type*/
proc sort data = ORIG.PHD_LU_ADMTYPE out = DEF.DEF_ADMTYPE presorted;
	by ADM_TYPE;
run; 

/*Charge master?*/
proc sort data = ORIG.PHD_LU_CHGMSTR out = DEF.DEF_CHGMST presorted;
	by std_chg_code;
run; 

/*CPT code*/
proc sort data = ORIG.PHD_LU_CPTCODE out = DEF.DEF_CPTCODE presorted;
	by CPT_CODE;
run; 

/*Discharge status*/
proc sort data = ORIG.PHD_LU_DISSTAT out = DEF.DEF_DISSTAT presorted;
	by DISC_STATUS;
run; 

/*Hospital Charges*/
proc sort data = ORIG.PHD_LU_HOSPCHG out = DEF.DEF_HOSPCHG presorted;
	by HOSP_CHG_ID;
run; 

/*ICD Code*/
proc sort data = ORIG.PHD_LU_ICDCODE out = DEF.DEF_ICDCODE presorted;
	by ICD_VERSION ICD_CODE;
run; 

/*ICD present on admit*/
proc sort data = ORIG.PHD_LU_ICDPOA out = DEF.DEF_ICDPOA presorted;
	by ICD_POA;
run; 

/*MSDRG*/
proc sort data = ORIG.PHD_LU_MSDRG out = DEF.DEF_MSDRG presorted;
	by MS_DRG;
run; 

/*MS DRG MDC*/
proc sort data = ORIG.PHD_LU_MSDRGMDC out = DEF.DEF_MSDRGMDC presorted;
	by MS_DRG_MDC;
run; 

/*Now bring in the patient type*/
proc sort data = ORIG.PHD_LU_PATTYPE out = DEF.DEF_PATTYPE presorted;
	by PAT_TYPE;
run;

/*Payor information*/
proc sort data = ORIG.PHD_LU_PAYOR out = DEF.DEF_PAYOR presorted;
	by STD_PAYOR;
run; 

/*Physician Specality*/
proc sort data = ORIG.PHD_LU_PHYSPEC out = DEF.DEF_PHYSPEC presorted;
	by PHY_SPEC;
run; 

/*Point of origin*/
proc sort data = ORIG.PHD_LU_POORIGIN out = DEF.DEF_POORIGIN presorted;
	by POINT_OF_ORIGIN;
run;

/*Providers*/
proc sort data = ORIG.PHD_LU_PROVIDERS out = DEF.DEF_PROVIDERS presorted;
	by PROV_ID;
run; 

