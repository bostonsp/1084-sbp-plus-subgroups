﻿/*-----------------------------------------------------------------------------
Project:            0867

Program:            0867_100_QC_Reporting.sas 

Function:           QC verification reports to check cohort variables

Dependencies:		____

Dependents:			____

Outputs:			____

Author:             ____

SAS Version:        ____   

Comments:           ____

Revisions:
date	name	what
200123	DKH		Initial version
-----------------------------------------------------------------------------*/
data reportingtbl / view=reportingtbl;
set SEP.SEL_SELECTION_FLAGS;
run;

/*	Check MD5 segments for 256 and patient count */
title "Check MD5 segments for 256 and patient count";
footnote "%sysfunc(datetime(), datetime25.)";
PROC SQL;
	SELECT
		COUNT(DISTINCT MD5_BYTE1) AS CDIST_MD5,
		COUNT(DISTINCT MEDREC_KEY) AS CDIST_PATIENTS,
		COUNT(DISTINCT PAT_KEY) AS CDIST_VISITS_PATKEY
	FROM
		reportingtbl
	;
QUIT;
title;
footnote;
/*
--------------------------------------------------------------------------------
Check MD5 segments for 256 and patient count 
CDIST_MD5 CDIST_PATIENTS CDIST_VISITS_PATKEY 
256 501825 545059 
*/

/*	Histogram DISC_MON */
footnote "%sysfunc(datetime(), datetime25.)";
proc univariate data=reportingtbl;
var DISC_MON;
histogram DISC_MON / normal;
inset
		N
		NMISS
		MEAN
		STD
		SKEWNESS
		KURTOSIS
		MIN
		P1
		P5
		P10
		Q1
		MEDIAN
		Q3
		P90
		P95
		P99
		MAX
		QRANGE
		MODE
		 / POSITION=NE
		CTEXT=BLUE
		CFILL=CXD3D3D3
		CFRAME=BLACK
		CHEADER=BLACK
		/*HEIGHT=2*/
	;
run;
title;
footnote;

/*	Histogram ADM_MON */
footnote "%sysfunc(datetime(), datetime25.)";
proc univariate data=reportingtbl;
var DISC_MON;
histogram DISC_MON / normal;
inset
		N
		NMISS
		MEAN
		STD
		SKEWNESS
		KURTOSIS
		MIN
		P1
		P5
		P10
		Q1
		MEDIAN
		Q3
		P90
		P95
		P99
		MAX
		QRANGE
		MODE
		 / POSITION=NE
		CTEXT=BLUE
		CFILL=CXD3D3D3
		CFRAME=BLACK
		CHEADER=BLACK
		/*HEIGHT=2*/
	;
run;
title;
footnote;

title "PROV_ID reports";
title2 "BEDS_GRP";
footnote "%sysfunc(datetime(), datetime25.)";
proc freq data=reportingtbl;
table PROV_ID / missing;
format PROV_ID BSRFMT.;
run;

title2 "URBAN_RURAL";
footnote "%sysfunc(datetime(), datetime25.)";
proc freq data=reportingtbl;
table PROV_ID / missing;
format PROV_ID URBRUR.;
run;

title2 "PROV_REGION";
footnote "%sysfunc(datetime(), datetime25.)";
proc freq data=reportingtbl;
table PROV_ID / missing;
format PROV_ID CREGFMT.;
run;

title2 "PROV_DIVISION";
footnote "%sysfunc(datetime(), datetime25.)";
proc freq data=reportingtbl;
table PROV_ID / missing;
format PROV_ID CDIVFMT.;
run;

title2 "COST_TYPE";
footnote "%sysfunc(datetime(), datetime25.)";
footnote2 "--- NOTE: END OF PROVIDER REPORTS --- ";
proc freq data=reportingtbl;
table PROV_ID / missing;
format PROV_ID COSTTYP.;
run;

title;
footnote;

/*	******************************************************	*/
title "MS_DRG reports";
footnote "%sysfunc(datetime(), datetime25.)";
proc freq data=reportingtbl;
table MS_DRG / missing;
format MS_DRG MSD.;
title;
footnote;
run;

/*	******************************************************	*/
title "ADM_SOURCE reports";
footnote "%sysfunc(datetime(), datetime25.)";
proc freq data=reportingtbl;
table ADM_SOURCE / missing;
format ADM_SOURCE $ADMS.;
title;
footnote;
run;

/*	******************************************************	*/
title "ADM_TYPE reports";
footnote "%sysfunc(datetime(), datetime25.)";
proc freq data=reportingtbl;
table ADM_TYPE / missing;
format ADM_TYPE $ATYPFMT.;
title;
footnote;
run;

/*	******************************************************	*/
title "DISC_STATUS reports";
footnote "%sysfunc(datetime(), datetime25.)";
proc freq data=reportingtbl;
table DISC_STATUS / missing;
format DISC_STATUS DISS.;
title;
footnote;
run;


/*	Histogram AGE */
ods graphics / height=11in width=17in;
footnote "%sysfunc(datetime(), datetime25.)";
proc univariate data=reportingtbl;
var AGE;
class GENDER;
histogram AGE / normal;
inset
		N
		NMISS
		MEAN
		STD
		SKEWNESS
		KURTOSIS
		MIN
		P1
		P5
		P10
		Q1
		MEDIAN
		Q3
		P90
		P95
		P99
		MAX
		QRANGE
		MODE
		 / POSITION=Nw
		CTEXT=BLUE
		CFILL=CXD3D3D3
		CFRAME=BLACK
		CHEADER=BLACK
		/*HEIGHT=2*/
	;
run;
title;
footnote;

/*	******************************************************	*/
title "ADM_PHY reports - ADMITTING";
footnote "%sysfunc(datetime(), datetime25.)";
proc freq data=reportingtbl;
table ADM_PHY / missing;
format ADM_PHY PHYS.;
title;
footnote;
run;

title "ATT_PHY reports - ATTENDING";
footnote "%sysfunc(datetime(), datetime25.)";
proc freq data=reportingtbl;
table ATT_PHY / missing;
format ATT_PHY PHYS.;
title;
footnote;
run;

/*	******************************************************	*/
title "STD_PAYOR reports";
footnote "%sysfunc(datetime(), datetime25.)";
proc freq data=reportingtbl;
table STD_PAYOR / missing;
format STD_PAYOR STDP.;
title;
footnote;
run;


/*	Histogram LOS */
ods graphics / height=11in width=17in;
title "LOS";
footnote "%sysfunc(datetime(), datetime25.)";
proc univariate data=reportingtbl;
var LOS;
histogram LOS / normal;
inset
		N
		NMISS
		MEAN
		STD
		SKEWNESS
		KURTOSIS
		MIN
		P1
		P5
		P10
		Q1
		MEDIAN
		Q3
		P90
		P95
		P99
		MAX
		QRANGE
		MODE
		 / POSITION=Nw
		CTEXT=BLUE
		CFILL=CXD3D3D3
		CFRAME=BLACK
		CHEADER=BLACK
		/*HEIGHT=2*/
	;
run;
title;
footnote;

title "Check validity indicators";
footnote "%sysfunc(datetime(), datetime25.)";

proc means data=reportingtbl;
var VALID:;
run;

title;
footnote;

/*	Histogram LOS */
ods graphics / height=11in width=17in;
title "PAT_COST";
footnote "%sysfunc(datetime(), datetime25.)";
proc univariate data=reportingtbl;
var PAT_COST;
histogram PAT_COST / normal;
inset
		N
		NMISS
		MEAN
		STD
		SKEWNESS
		KURTOSIS
		MIN
		P1
		P5
		P10
		Q1
		MEDIAN
		Q3
		P90
		P95
		P99
		MAX
		QRANGE
		MODE
		 / POSITION=Nw
		CTEXT=BLUE
		CFILL=CXD3D3D3
		CFRAME=BLACK
		CHEADER=BLACK
		/*HEIGHT=2*/
	;
run;
title;
footnote;
