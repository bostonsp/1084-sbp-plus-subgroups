﻿/*-----------------------------------------------------------------------------
Project:            0867

Program:            0867_00a_Common_FMTS.sas 

Function:           Create formats from Premier DEF tables into libname FMT0867

Dependencies:		____

Dependents:			____

Outputs:			____

Author:             ____

SAS Version:        ____   

Comments:           ____

Revisions:
date	name	what
200123	DKH		Added more fmts
200123	DKH		Initial version, move BSRFMT and ATYPFMT from 03.sas to here
-----------------------------------------------------------------------------*/
/*	Change libname to readwrite 	*/
libname FMT0867 CLEAR;
libname FMT0867 V9 "F:\0867\FMT0867"; /* comment this line out to avoid inadvertent overwrite */

options fmtsearch=(FMT0867 WORK LIBRARY);

/*Start by creating a format for the providers, specifically bed size 
and create a format for the admission type.*/ 
%include "C:\Users\&SYSUSERID.\Documents\0867-grifols-sepsis-and-cirrhosis-analysis-in-premier\Macros\mcr_format_from_hd.sas";
%mcr_format_from_hd(fmt_name = BSRFMT,
	dict_tbl = DEF.DEF_PROVIDERS,
 	id_field = PROV_ID,
 	desc_field = BEDS_GRP,
	fmt_lib=FMT0867); 
%mcr_format_from_hd(fmt_name = URBRUR,
	dict_tbl = DEF.DEF_PROVIDERS,
 	id_field = PROV_ID,
 	desc_field = URBAN_RURAL,
	fmt_lib=FMT0867); 
%mcr_format_from_hd(fmt_name = CREGFMT,
	dict_tbl = DEF.DEF_PROVIDERS,
 	id_field = PROV_ID,
 	desc_field = PROV_REGION,
	fmt_lib=FMT0867); 
%mcr_format_from_hd(fmt_name = CDIVFMT,
	dict_tbl = DEF.DEF_PROVIDERS,
 	id_field = PROV_ID,
 	desc_field = PROV_DIVISION,
	fmt_lib=FMT0867); 
%mcr_format_from_hd(fmt_name = COSTTYP,
	dict_tbl = DEF.DEF_PROVIDERS,
 	id_field = PROV_ID,
 	desc_field = COST_TYPE,
	fmt_lib=FMT0867); 

%mcr_format_from_hd(fmt_name = ATYPFMT,
	dict_tbl = DEF.DEF_admtype,
 	id_field = ADM_TYPE,
 	desc_field = ADM_TYPE_DESC,
	fmt_lib=FMT0867); 

%mcr_format_from_hd(fmt_name = DISS,
	dict_tbl = DEF.DEF_DISSTAT,
 	id_field = DISC_STATUS,
 	desc_field = DISC_STATUS_DESC,
	fmt_lib=FMT0867); 

%mcr_format_from_hd(fmt_name = MSD,
	dict_tbl = DEF.DEF_MSDRG,
 	id_field = MS_DRG,
 	desc_field = MS_DRG_DESC,
	fmt_lib=FMT0867); 

%mcr_format_from_hd(fmt_name = STDP,
	dict_tbl = DEF.DEF_PAYOR,
 	id_field = STD_PAYOR,
 	desc_field = STD_PAYOR_DESC,
	fmt_lib=FMT0867); 

%mcr_format_from_hd(fmt_name = PHYS,
	dict_tbl = DEF.DEF_PHYSPEC,
 	id_field = PHY_SPEC,
 	desc_field = PHY_SPEC_DESC,
	fmt_lib=FMT0867); 

%mcr_format_from_hd(fmt_name = TEACHFMT,
	dict_tbl = DEF.DEF_PROVIDERS,
 	id_field = PROV_ID,
 	desc_field = TEACHING,
	fmt_lib=FMT0867); 

%mcr_format_from_hd(fmt_name = PATTYP,
	dict_tbl = DEF.DEF_PATTYPE,
 	id_field = PAT_TYPE,
 	desc_field = PAT_TYPE_DESC,
	fmt_lib=FMT0867);


/*	ADMSRC - BEGIN	*/
proc sort data=DEF.DEF_ADMSRC out=_admsrc NODUPKEY PRESORTED;
by ADM_SOURCE ADM_SRC_DESC;
run;

data _cntl_adm;
	retain
		FMTNAME '$ADMS'
		TYPE 'C'
	;
	set
		_admsrc(
			rename=(
				ADM_SOURCE = START
				ADM_SRC_DESC = LABEL
			)
		) END=DONE
	;
	by START LABEL;

	output;

	if (DONE) then do;
		LABEL = 'NO DESC';
		HLO = 'O';
	end;
run;

proc format cntlin=work._cntl_adm lib=FMT0867;
run;

/*	verify fmt */
data _verif;
	set DEF.DEF_ADMSRC;
	_fmtd = put(ADM_SOURCE , $ADMS. );
run;

proc print data=_verif(obs=100);
run;

/*	ADMSRC - END 	*/

/*	CPTCODE - BEGIN	*/
proc sort data=DEF.DEF_CPTCODE out=_cptcode NODUPKEY PRESORTED;
by _ALL_;
run;

data _cntl_cpt;
	retain
		FMTNAME '$CPTC'
		TYPE 'C'
	;
	set
		_cptcode(
			rename=(
				CPT_CODE = START
				CPT_DESC = LABEL
			)
		) END=DONE
	;
	by START LABEL;

	output;

	if (DONE) then do;
		LABEL = 'NO DESC';
		HLO = 'O';
	end;
run;

proc format cntlin=work._cntl_cpt lib=FMT0867;
run;

/*	verify fmt */
data _verif;
	set DEF.DEF_CPTCODE;
	_fmtd = put(CPT_CODE , $CPTC. );
run;

proc print data=_verif(obs=100);
run;

/*	CPTCODE - END 	*/

/*	ICDPOA - BEGIN	*/
proc sort data=DEF.DEF_ICDPOA out=_icdpoa NODUPKEY PRESORTED;
by _ALL_;
run;

data _cntl_icdp;
	retain
		FMTNAME '$ICDP'
		TYPE 'C'
	;
	set
		_icdpoa(
			rename=(
				ICD_POA = START
				ICD_POA_DESC = LABEL
			)
		) END=DONE
	;
	by START LABEL;

	output;

	if (DONE) then do;
		LABEL = 'NO DESC';
		HLO = 'O';
	end;
run;

proc format cntlin=work._cntl_ICDP lib=FMT0867;
run;

data _cntl_icdps;
	retain
		FMTNAME '$ICDPS'
		TYPE 'C'
	;
	set
		_icdpoa(
			rename=(
				ICD_POA = START
				ICD_POA_SUM_DESC = LABEL
			)
		) END=DONE
	;
	by START LABEL;

	output;

	if (DONE) then do;
		LABEL = 'NO DESC';
		HLO = 'O';
	end;
run;

proc format cntlin=work._cntl_ICDPS lib=FMT0867;
run;

/*	verify fmt */
data _verif;
	set DEF.DEF_ICDPOA;
	_fmtd = put(ICD_POA , $ICDP. );
	_fmtd2 = put(ICD_POA, $ICDPS.);
run;

proc print data=_verif(obs=100);
run;

/*	ICDPOA - END 	*/


/*	ICD CODE 	*/
proc sort data=DEF.DEF_ICDCODE out=_icdcode NODUPKEY PRESORTED;
by _ALL_;
run;

data _cntl_icd;
	retain
		FMTNAME '$ICDC'
		TYPE 'C'
	;
	length
		START $ 16
		LABEL $ 256
	;
	set
		_icdcode(
			keep=
				ICD_VERSION
				ICD_CODE
				ICD_DESC
		) END=DONE
	;
	by
		ICD_VERSION
		ICD_CODE
		ICD_DESC
	;

	START = strip(cats(strip(put(ICD_VERSION,2.)),"|",strip(ICD_CODE)));
	LABEL = strip(ICD_DESC);

	output;

	if (DONE) then do;
		LABEL = 'NO DESC';
		HLO = 'O';
	end;
run;

proc format cntlin=work._cntl_icd lib=FMT0867;
run;

/*	verify fmt */
data _verif;
	set DEF.DEF_ICDCODE;
	START = strip(cats(strip(put(ICD_VERSION,2.)),"|",strip(ICD_CODE)));
	_fmtd = put(START , $ICDC. );
run;

proc print data=_verif(obs=100);
run;

/*	Change libname to readonly 	*/
libname FMT0867 CLEAR;
libname FMT0867 V9 "F:\0867\FMT0867" ACCESS=READONLY;
