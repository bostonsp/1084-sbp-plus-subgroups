﻿/*-----------------------------------------------------------------------------
Project:            ____

Program:            0867_05_Import_PATCPT.sas

Function:           ____

Dependencies:		GR_YYYYM_PATCPT.SAS7BDAT
					key table NOAPR.MEDREC_KEYTABLE

Dependents:			____

Outputs:			____

Author:             ____

SAS Version:        ____   

Comments:           ____

Revisions:
date	name	what
191206	XM		Updated for the new data that was sent in November 2019
191120	XM		First version
-----------------------------------------------------------------------------*/
libname ORIG V9 "G:\PAS23" access=readonly;
libname NOAPR V9 "S:\0867_PREMIER\201911\NOAPR\" access = readonly;
libname CPT V9 "S:\0867_PREMIER\201911\CPT\";

/*	select memname metadata into global macro variable for macro later on	*/
PROC SQL NOPRINT;
	SELECT
		DISTINCT MEMNAME into :mnames SEPARATED BY ' '
	FROM
		SASHELP.VCOLUMN
	WHERE
		STRIP(LIBNAME) EQ 'ORIG'
		AND UPCASE(STRIP(MEMNAME)) CONTAINS 'PATCPT'
	;
QUIT;
%put &mnames.;

proc contents data = ORIG.PHD_20161_PATCPT varnum;
run; 

%macro mac_cpt;
%global &mnames;
%local i memcount thismem j;

%let memcount=%sysfunc(countw(%superq(mnames)));
%put NOTE: &memcount.;
data
	%do j=0 %to 255;
		CPT.CPT_%unquote(&j.)
	%end;
;

	if (1 EQ 0) then do;
		set NOAPR.MEDREC_KEYTABLE;
		set ORIG.PHD_20161_PATCPT;
	end;

	if (_N_ EQ 1) then do;

		declare hash hk(dataset:"NOAPR.MEDREC_KEYTABLE");
		rc=hk.defineKey('PAT_KEY');
		rc=hk.defineData('PAT_KEY','MEDREC_KEY','MD5_BYTE1');
		rc=hk.defineDone();

		/*	declare fact table	*/
		declare hash hf(ordered:'A',hashexp:10);
		declare hiter hfi('hf');
		rc1=hf.defineKey(
			'MD5_BYTE1'
			,'MEDREC_KEY'
			,'PAT_KEY'
			,'CPT_CODE'
		);
		rc1=hf.defineData(
			'MD5_BYTE1'
			,'MEDREC_KEY'
			,'PAT_KEY'
			,'CPT_CODE'
		);
		rc1=hf.defineDone();

	end;

	set
		%do i=1 %to &memcount.;
		%let thismem = %sysfunc(scan(%superq(mnames),%unquote(&i.)));
			ORIG.%unquote(&thismem.)/*(obs=10000)*/
		%end;
		END=DONE
	;

	rc=hk.find(key:PAT_KEY);

	if (rc EQ 0) then do;
		/*MD5_BYTE1 = rank (char (md5 (strip (MEDREC_KEY)), 1));*/
		rc=hf.replace();
	end;

	if (DONE) then do;
		/*rc1=hf.output(dataset:"work.DIAG(compress=no)");*/
		rc1=hfi.first();
		do while (rc1 EQ 0);
			%do j=0 %to 255;
				%if &j. GT 0 %then %do;
				else
				%end;
				if (MD5_BYTE1 EQ &j.) then output CPT.CPT_%unquote(&j.);
			%end;
			rc1=hfi.next();
		end;
	end;

	drop
		rc
		rc1
	;
run;
%if &syserr. GT 4 %then %abort;
%mend mac_cpt;
%mac_cpt;


%macro setmeta;
%local j;
%do j=0 %to 255;
	proc sort data=CPT.CPT_%unquote(&j.) nodupkey presorted;
	by _ALL_
	;
	run;
	%if &syserr. GT 4 %then %abort;
%end;
%mend setmeta;
%setmeta;
