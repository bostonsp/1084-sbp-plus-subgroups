﻿/*---------------------------------------------------------------------------------
Project:            1084

Program:            ____

Function:           ____

Dependencies:		____

Dependents:			____

Author:             ____

Date Completed:     ____

SAS Version:        ____   

Platform:           ____

Macros Called:      ____ 

Macro Parameters:   ____

Comments:           ____

Example:			____

Revisions:
date	name	what
211017	SH		Initial Version
---------------------------------------------------------------------------------*/

libname DEF "S:\PAS14\DEF" access=readonly;
libname DEMO "S:\PAS14\DEMO" access=readonly; 
libname CPT "S:\PAS14\CPT" access=readonly; 
libname DIAG "S:\PAS14\DIAG" access=readonly; 
libname PROC "S:\PAS14\PROC" access=readonly; 
libname BILL "S:\PAS14\BILL" access=readonly; 
libname VITALS "S:\PAS14\VITALS" access=readonly; 
libname GENLAB "S:\PAS14\GENLAB" access=readonly; 
libname LAB_RES "S:\PAS14\LAB_RES" access=readonly; 
libname LAB_SENS "S:\PAS14\LAB_SENS" access=readonly; 
libname READMIT "S:\PAS14\READMIT" access=readonly; 

libname dtout "F:\1084\Data";

**************************************************  End  **************************************************;
